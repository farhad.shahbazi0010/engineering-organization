﻿<!doctype html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="سامانه آنلاین خدمات مهندسی ساختمان">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>سامانه آنلاین خدمات مهندسی ساختمان</title>
    <link rel="stylesheet" href="{{asset('asset/css/base.css')}}">
    <link rel="stylesheet" href="{{asset('asset/css/vendor.css')}}">
    <link rel="stylesheet" href="{{asset('asset/css/font-awesome/css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" href="{{asset('asset/bootstrap-4.5.0-dist/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('asset/css/jquery.toast.css')}}">
    <link rel="stylesheet" href="{{asset('asset/css/main.css')}}">

{{--    <script src="{{asset('asset/js/modernizr.js')}}"></script>--}}
    @yield('head')
</head>
<body id="top">

{{--<div id="preloader">--}}
{{--    <div id="loader" class="dots-fade">--}}
{{--        <div></div>--}}
{{--        <div></div>--}}
{{--        <div></div>--}}
{{--    </div>--}}
{{--</div>--}}

<!-- header

================================================== -->


<header class="s-header header">

    <div class="container-fluid headertop">
        <div class="row">
            <div class="col text-right">
                <a href="https://www.tceo.ir"><img src="{{asset('asset/images/logo-8.jpg')}}" alt="سازمان نظام مهندسی ساختمان استان تهران"></a>
            </div>
            <div class="col-5 text-center nastaliq">
                <h1 class="nastaliq" style="font-weight: normal">سامانه آنلاین خدمات مهندسی ساختمان</h1>
            </div>
            <div class="col text-left">
                @auth()
                    <a href="#" class="btn btn-danger btn-lg" onclick="
                    document.getElementById('logout').submit()">
                        خروج
                    </a>
                @if(auth()->user()->role== 'admin')  <a class="btn btn-danger btn-lg" href="/admin/ads">پنل مدیریت</a> @endif
                    <form id="logout" action="{{route('logout')}}" method="post">@csrf</form>

                @else
                    <a href="/" style="font-size: 17px;font-weight:bold;position: relative;bottom: 9px;"><span style="color: #e07165 ">EJ</span> <span style="color: #4f56a3">RA</span></a>
                @endauth
            </div>
        </div>
    </div>



    <a class="header__toggle-menu" href="#0" title="Menu"><span>Menu</span></a>
    <nav class="header__nav-wrap">

        <h2 class="header__nav-heading h6" style="font-family: Tahoma; letter-spacing: 0">سامانه آنلاین خدمات ساختمانی اجرا</h2>

        <ul class="header__nav">
            <li class="current"><a href="/" title="">صفحه اصلی</a></li>
            <li class="has-children">
                <a href="#0" title="">مالکین (کارفرمایان)</a>
                <ul class="sub-menu">
                    <li><a href="/advertisement">درخواست مجری ذی صلاح</a></li>
                </ul>
            </li>
            <li class="has-children">
                <a href="#0" title="">مجریان (حقوقی و حقیقی)</a>
                <ul class="sub-menu">
                    <li><a href="/loginRegister">عضویت یا ورود</a></li>
                </ul>
            </li>
            <li><a href="/anjoman" title="">انجمن سازندگان ساختمان</a></li>
            <li><a href="/news" title="">اخبار و اطلاعیه ها</a></li>
        </ul> <!-- end header__nav -->

        <a href="#0" title="Close Menu" class="header__overlay-close close-mobile-menu">Close</a>

    </nav> <!-- end header__nav-wrap -->

</header> <!-- s-header -->
