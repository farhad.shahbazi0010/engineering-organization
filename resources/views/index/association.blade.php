@extends('layout')

@section('content')

    <div class="printable">
        <div class="row">
            <div class="col-12 text-right adsTitle">
                <span>{{$asso->title}}</span>
            </div>
        </div>

        {{--    <img src="{{asset('associationfolder/association.jpg')}}" alt="">--}}
        <div class="row">
            <div class="col-12 anjoman">
                {!! $asso->text !!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 ">
            <button class="btn btn-lg btn-secondary" id="print">
                پرینت
                <i class="fa fa-print"></i>
            </button>
            <button class="btn btn-lg btn-info" id="share" data-toggle="modal" data-target="#sharemodal">
                اشتراک گزاری
                <i class="fa fa-share"></i>
            </button>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" style="direction: ltr" id="sharemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="margin: 0; font-weight: 400">اشتراک گزاری</h5>
                </div>
                <div class="modal-body align-center">
                    <a href="tg://msg_url?url=http://www.ejra.ir/anjoman">
                        <img src="{{asset('asset/images/telegram.png')}}" style="width: 35%" alt="">
                    </a>
                    <a href="whatsapp://send?text=http://www.ejra.ir/anjoman">
                        <img src="{{asset('asset/images/whatsapp.jpg')}}" style="width: 35%" alt="">
                    </a>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('foot')
    <script src="{{asset('asset/jQuery.print.js')}}"></script>
    <script>
        $('<div style="overflow-x: auto" class="tableparent"></div>').insertAfter('.anjoman table')
        $('.anjoman table').each(function () {
            var afterElement= $(this).next('.tableparent')
            $(this).appendTo(afterElement)
        })

        $('button#print').click(function () {

            $(".printable").print({
                title : 'Ejra'
            });
        })

    </script>
@endsection

