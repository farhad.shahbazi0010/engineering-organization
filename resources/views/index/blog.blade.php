@extends('layout')

@section('content')
    <div class="row">
        <div class="col-12 text-right adsTitle">
            <span>اخبار و اطلاعیه ها</span>
        </div>
    </div>

    <div style="position: relative;" id="ads">
        @foreach($blogs as $blog)
            <div class="jumbotron">
                <h3 class="display-4">{{$blog->title}}</h3>
                <hr class="my-4">
                <div class="row">
                    <div class="col-md-4 text-center mb-4" style="max-height: 262px;max-width: 337px">
                        <img src="{{asset('blog/'.$blog->id.'/blog_'.$blog->id.'.jpg')}}"  alt="">
                    </div>
                    <div class="col-md-8">
                        <div class="lead">
                            {!! $blog->text !!}
                        </div>

                    </div>
                </div>
                <hr class="my-4">
                <div class="text-left"><a class="btn btn-info btn-lg" href="/news/{{$blog->id}}" role="button">ادامه مطلب</a></div>
            </div>
        @endforeach

            <div class="loading adsLoading" style="display: none;position: absolute;top: 0;right: 0;left: 0;bottom: 0;background-color: rgba(0,0,0,.2);">
                <div class="spinner-border " style="position:absolute;top: 93%;right: 50%;height: 50px;width: 50px"></div>
            </div>

    </div>



  @if($count)
      <div class="row pagination-wrap adsPagination">
          <div class="col-12">
              <div class="col-full">
                  <nav class="pgn">
                      <ul>
                          <li><a class="pgn__prev nxt" href="#0">Prev</a></li>
                          @for($i= 0; $i< ceil($count/6); $i++ )
                              @if($i== 0)
                                  <li><span class="pgn__num current">{{$i+1}}</span></li>
                              @else
                                  <li><a class="pgn__num" href="#0">{{$i+1}}</a></li>
                              @endif
                          @endfor
                          <li><a class="pgn__next prv" href="#0">Next</a></li>
                      </ul>
                  </nav>
              </div>
          </div>
      </div>
  @endif
@endsection

@section('foot')
    <script>

        $.ajaxSetup({
            headers: {
                'X_CSRF_TOKEN': $('meta[name=csrf-token]').attr('content')
            }
        })

        var pageNumber

        // ads pagination-----------------------------------

        $(document).on('click','.adsPagination a.pgn__num',function(e){
            e.preventDefault()
            var current= $('.adsPagination .current')
            pageNumber= $(this).text()

            current.parent().html('<a class="pgn__num" href="#0">'+current.text()+'</a>')
            $(this).parent().html('<span class="pgn__num current">'+pageNumber+'</span>')
            $('.adsLoading').fadeIn()
            getadvertisement(pageNumber)


        })
        $(document).on('click','.adsPagination a.nxt',function(e){
            e.preventDefault()
            var current= $('.adsPagination .current')
            var target= current.parent().next('li').children('.pgn__num')
            if(target.length)
            {
                pageNumber= target.text()
                current.parent().html('<a class="pgn__num" href="#0">'+current.text()+'</a>')
                target.parent().html('<span class="pgn__num current">'+pageNumber+'</span>')
                $('.adsLoading').fadeIn()
                getadvertisement(pageNumber)

            }

        })
        $(document).on('click','.adsPagination a.prv',function(e){
            e.preventDefault()
            var current= $('.adsPagination .current')
            var target= current.parent().prev('li').children('.pgn__num')
            if(target.length)
            {
                pageNumber= target.text()
                current.parent().html('<a class="pgn__num" href="#0">'+current.text()+'</a>')
                target.parent().html('<span class="pgn__num current">'+pageNumber+'</span>')
                $('.adsLoading').fadeIn()
                getadvertisement(pageNumber)
            }

        })


        function getadvertisement(pagenumber) {
            $.ajax({
                url: '/blogPagination/'+pagenumber,
                method: 'post',
                cache: false,
                data:{showItem:0},
                success: function (msg) {

                        var ads= $('#ads')


                        if(ads.children('.jumbotron').length)
                        {
                            ads.children('.jumbotron').remove()
                        }

                        ads.prepend(msg)



                    $('.loading').fadeOut()
                },
                error: function (msg) {
                    console.log(msg)
                }
            })
        }
    </script>
@endsection
