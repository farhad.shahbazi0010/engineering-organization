@extends('layout')
@section('head')
    <link rel="stylesheet" href="{{asset('asset/css/styleLogin.css')}}">
@endsection
@section('content')
    <div class="form-v8">

        <div class="page-content">

            <div class="form-v8-content">


                <!--    Content Right    -->

                <div class="form-right">

                    <div class="tab">

                        <div class="tab-inner">
                            <button class="tablinks signup" onclick="openCity(event, 'sign-up')" id="defaultOpen">ثبت نام</button>
                        </div>

                        <div class="tab-inner">
                            <button class="tablinks signin" onclick="openCity(event, 'sign-in')">ورود</button>
                        </div>

                    </div>


                    <!--        Register Form            -->

                    <form class="form-detail" action="{{route('register')}}" method="post">
                        @csrf
                        <div class="tabcontent" id="sign-up">


                            <!--  Name Input  -->

                            <div class="form-row">
                                <label class="form-row-inner">
                                    <input type="text" name="fullname" id="fullname" class="input-text" required value="{{old('fullname')}}">
                                    <span class="label">نام و نام خانوادگی مهندس یا اسم شرکت</span>
                                    @error('fullname')
                                    <div style="display: inline-block;" class="invalid-feedback myval">{{$message}}</div>
                                    @enderror

                                </label>
                            </div>


                            <!--  Tel Input  -->

                            <div class="form-row">
                                <label class="form-row-inner">
                                    <input type="text" name="phone" id="phone" class="input-text" required value="{{old('phone')}}">
                                    <span class="label">شماره همراه</span>
                                    @error('phone')
                                    <div style="display: inline-block;" class="invalid-feedback myval">{{$message}}</div>
                                    @enderror
                                </label>
                            </div>


                            <!--  Password Input  -->

                            <div class="form-row">
                                <label class="form-row-inner">
                                    <input type="password" name="password" id="password" class="input-text" required>
                                    <span class="label">رمز عبور</span>
                                    @error('password')
                                    <div style="display: inline-block;" class="invalid-feedback myval">{{$message}}</div>
                                    @enderror
                                </label>
                            </div>


                            <!--  Comfirm Password Input  -->

                            <div class="form-row">
                                <label class="form-row-inner">
                                    <input type="password" name="password_confirmation" id="password_confirmation" class="input-text" required>
                                    <span class="label">تکرار رمز عبور</span>
                                    @error('password_confirmation')
                                    <div style="display: inline-block;" class="invalid-feedback myval">{{$message}}</div>
                                    @enderror
                                </label>
                            </div>


                            <!--  Captcha Code  -->

                            <div class="form-row">

                                <label class="form-row-inner">

                                    <style>
                                        .flex-container_cap {
                                            width: 100%;
                                            display: flex;
                                        }

                                        .flex-Item_cap {
                                            width: 50%;
                                        }
                                        ::placeholder {
                                            color: #004c72;
                                            opacity: 1;
                                            font-size: 14px;
                                        }

                                        :-ms-input-placeholder {
                                            color: #ffffff;
                                        }

                                        ::-ms-input-placeholder {
                                            color: #ffffff;
                                        }
                                    </style>

                                    <div class="flex-container_cap">

                                        <div class="flex-Item_cap">
                                            <div class="container_city_reg_Item ">
                                                <div class="captcha">
                                                    {!! captcha_img('math') !!}
                                                </div>
                                                <span class="btn btn-info refresh"><i class="fa fa-retweet"></i></span>
                                            </div>
                                        </div>


                                        <!--  Captcha Input  -->

                                        <div class="flex-Item_cap">

                                            <div class="container_city_reg_Item">
                                                <input style="direction: ltr" type="text" placeholder="کد امنیتی" class="w_120" name="captcha" id="captcha" required>
                                                @error('captcha')
                                                <div style="display: inline-block;" class="invalid-feedback myval">{{$message}}</div>
                                                @enderror
                                            </div>

                                        </div>
                                    </div>

                                </label>

                            </div>

                            <!--           Sumbmit Btn             -->

                            <div class="form-row-last">
                                <!--                                <input type="submit" name="register" class="btn btn-lg btn-info" value="ثبت نام">-->
                                <button class="btn btn-lg btn-info w-100 mt-5">ثبت نام</button>
                            </div>

                        </div>
                    </form>


                    <!--    Sign In Form     -->

                    <form class="form-detail" action="{{route('login')}}" method="post">
                        @csrf

                        <div class="tabcontent" id="sign-in">


                            <!--    Sign In Tel Input    -->

                            <div class="form-row">

                                <label class="form-row-inner">
                                    <input type="text" name="phone2" id="full_name_1" class="input-text" required value="{{old('phone2')}}">
                                    <span class="label">شماره همراه</span>
                                    <span class="border"></span>
                                </label>

                            </div>


                            <!--    Sign In Password Input    -->

                            <div class="form-row">

                                <label class="form-row-inner">
                                    <input type="password" name="password" id="password_1" class="input-text" required>
                                    <span class="label">رمز عبور</span>
                                    <span class="border"></span>
                                </label>

                            </div>

                            <!--    Sign In Btn    -->
                            @error('credential')
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <strong>{{$message}}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @enderror

                            <div class="form-row-last">
                                <!--                                <input type="submit" name="register" class="register" value="ورود">-->
                                <button class="btn btn-lg btn-info w-100">ورود</button>
                            </div>

                        </div>
                    </form>
                </div>


                <!--    Content Left    -->

                <div class="form-left">
                    <img src="{{asset('asset/images/Home.jpg')}}" alt="form">
                </div>

            </div>

        </div>


        <!--    Sign In and Register (Tab)    -->


    </div>
@endsection
@section('foot')
    <script src="{{asset('asset/js/bootstrap-validate-2.2.0/dist/bootstrap-validate.js')}}"></script>
    <script>



        $('.refresh').click(function () {
            $.ajax({
                method: 'get',
                url: '/refreshcaptcha',
                dataType: 'json',
                success: function (msg) {
                    $('.captcha').html(msg.captcha)

                }
            })
        })

        $('input').on('keydown',function () {
            if ($(this).parent().find('.myval').length)
                $(this).parent().find('.myval').remove()
        })


            function openCity(evt, cityName) {
                var i, tabcontent, tablinks;
                tabcontent = document.getElementsByClassName("tabcontent");
                for (i = 0; i < tabcontent.length; i++) {
                    tabcontent[i].style.display = "none";
                }
                tablinks = document.getElementsByClassName("tablinks");
                for (i = 0; i < tablinks.length; i++) {
                    tablinks[i].className = tablinks[i].className.replace(" active", "");
                }
                document.getElementById(cityName).style.display = "block";
                evt.currentTarget.className += " active";
            }

        // Get the element with id="defaultOpen" and click on it
        document.getElementById("defaultOpen").click();



        // if login was wrong the signIn page show rather than signUp
        @error('credential')
        $('.signin').addClass('active')
        $('.signup').removeClass('active')
        $('#sign-up').hide()
        $('#sign-in').show()
        @enderror



        bootstrapValidate('#fullname','required:فیلد بالا را پر کنید')
        bootstrapValidate('#phone','required:فیلد بالا را پر کنید|regex:^0[0-9]{10}$: شماره تلفن معتبر وارد کنید ')
        bootstrapValidate('#password','required:فیلد بالا را پر کنید|min:5:حداقل 5 کاراکتر وارد کنید')
        bootstrapValidate('#password_confirmation','required:فیلد بالا را پر کنید|matches:#password:رمز عبور یکسان وارد کنید')
        bootstrapValidate('#captcha','required:فیلد بالا را پر کنید')

    </script>
@endsection
