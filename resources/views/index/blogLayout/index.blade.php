@foreach($blogs as $blog)
    <div class="jumbotron">
        <h3 class="display-4">{{$blog->title}}</h3>
        <hr class="my-4">
        <div class="row">
            <div class="col-md-4 text-center mb-4" style="max-height: 262px;max-width: 337px">
                <img src="{{asset('blog/'.$blog->id.'/blog_'.$blog->id.'.jpg')}}"  alt="">
            </div>
            <div class="col-md-8">
                <div class="lead">
                    {!! $blog->text !!}
                </div>

            </div>
        </div>
        <hr class="my-4">
        <div class="text-left">
            @if($showItem==1)
                <button class="btn btn-primary btn-sm" id="showAll" href="#" role="button">نمایش کامل</button>
                <button class="btn btn-info btn-sm" id="edit" href="#" role="button">ویرایش</button>
                <button class="btn btn-danger btn-sm" id="delete" href="#" role="button" style="position: relative">
                    حذف
                    <div id="buttonSpinner">
                        <div class="spinner-border"></div>
                    </div>
                </button>
            @else
                <a class="btn btn-info btn-lg" href="/news/{{$blog->id}}" role="button">ادامه مطلب</a>
            @endif

        </div>
    </div>
@endforeach
