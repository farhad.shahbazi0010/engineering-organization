﻿@extends('layout')

@section('content')
    <div class="row_con" style="padding-bottom: 25px;">فرم درخواست مجری ذی صلاح</div>

    <div class="flex-container_con NO formdiv">

        <div class="flex-Item_con w-100">

            <form action="/saveAds" method="post" id="adsForm">
                @csrf

                <div class="form-group row">

                    <!--  Name Input  -->

                    <div class="form-group col-md-6">
                        <label for="inputName">نام : </label>
                        <input type="text" class="form-control" id="inputName" name="name" placeholder="نام" required value="{{old('name')}}">
                        @error('name')
                        <div style="display: inline-block;" class="invalid-feedback myval">{{$message}}</div>
                        @enderror
                    </div>

                    <!--  Family Input  -->

                    <div class="form-group col-md-6">
                        <label for="inputFamily">نام خانوادگی : </label>
                        <input type="text" name="lastname" class="form-control" id="inputFamily" placeholder="نام خانوادگی " required value="{{old('lastname')}}">
                        @error('lastname')
                        <div style="display: inline-block;" class="invalid-feedback myval">{{$message}}</div>
                        @enderror
                    </div>

                </div>

                <div class="form-group row">

                    <!--  Area Input  -->

                    <div class="form-group col-md-6">
                        <label for="inputArea">متراژ : </label>
                        <span class="errmsg mr-5" style="font-size: 10pt"></span>
                        <input type="text" name="area" class="form-control inputnumber" id="inputArea" placeholder="متراژ (مترمربع) " required value="{{old('area')}}">
                        @error('area')
                        <div style="display: inline-block;" class="invalid-feedback myval">{{$message}}</div>
                        @enderror
                    </div>


                    <div class="form-group col-md-6">
                        <!---------------------------------------->
                        <label for="inputCstage">مرحله ساختمانی : </label>
                        <select class="browser-default custom-select w-100" name="cstage" style="height: 62%" required>
                            <option @if(old('cstage')== 'شروع به کار') selected @endif  value="شروع به کار">شروع به کار</option>
                            <option @if(old('cstage')== 'فوندانسیون') selected @endif  value="فوندانسیون">فوندانسیون</option>
                            <option @if(old('cstage')== 'در حال ساخت') selected @endif  value="در حال ساخت">در حال ساخت</option>
                            <option @if(old('cstage')== 'سفت کاری') selected @endif  value="سفت کاری">سفت کاری</option>
                            <option @if(old('cstage')== 'نازک کاری') selected @endif  value="نازک کاری">نازک کاری</option>
                            <option @if(old('cstage')== 'پایان کار') selected @endif  value="پایان کار">پایان کار</option>
                        </select>
                        @error('cstage')
                        <div style="display: inline-block;" class="invalid-feedback myval">{{$message}}</div>
                        @enderror
                    </div>

                </div>


                <div class="form-group row">

                    <!--  Tel Input  -->

                    <div class="form-group col-md-6">
                        <label for="inputceiling">تعداد سقف : </label>
                        <span class="errmsg mr-5" style="font-size: 10pt"></span>
                        <input type="text" name="ceil" class="form-control inputnumber" id="inputceiling" placeholder="تعداد سقف  " required value="{{old('ceil')}}">
                        @error('ceil')
                        <div style="display: inline-block;" class="invalid-feedback myval">{{$message}}</div>
                        @enderror
                    </div>

                    <!--  Manfi Input  -->

                    <div class="form-group col-md-6">
                        <label for="inputManfi">تعداد منفی :</label>
                        <span class="errmsg mr-5" style="font-size: 10pt"></span>
                        <input type="text" name="negative" class="form-control inputnumber" id="inputManfi" placeholder="تعداد منفی " required value="{{old('negative')}}">
                        @error('negative')
                        <div style="display: inline-block;" class="invalid-feedback myval">{{$message}}</div>
                        @enderror
                    </div>

                </div>



                <div id="err_fild" class="form-group row text-center">

                    <div class="container_city_reg_state">

                        <div class="container_city_reg_Item pt-3 col-md-6" style="text-align: right">

                            <!--Area select-->

                            <select name="city_id" class="selectItem ml-1 mr-4" id="city" style="margin-top: 24px ; width: 40%">
                                @foreach($cities as $city)
                                    <option @if(old('city_id')==$city->id) selected @endif value="{{$city->id}}">{{$city->title}}</option>
                                @endforeach
                            </select>
                            <select class="selectItem mt-3" name="region_id" id="state">
                                @foreach($cities[0]->regions as $tehranRegion)
                                    <option @if(old('region_id')==$tehranRegion->id) selected @endif value="{{$tehranRegion->id}}">{{$tehranRegion->title}}</option>
                                @endforeach
                            </select>
                            @error('city_id')
                            <div style="display: inline-block;" class="invalid-feedback myval">{{$message}}</div>
                            @enderror

                        </div>




                        <!--   Haghighi or Hoghoghi    -->

{{--                        <div class="container_city_reg_Item">--}}

{{--                            <select name="type" class="browser-default custom-select mt-5" style="height: 62%" required>--}}
{{--                                <option value="حقیقی">حقیقی</option>--}}
{{--                                <option value="حقوقی">حقوقی</option>--}}
{{--                                <option selected value="حقیقی و حقوقی">حقیقی و حقوقی</option>--}}
{{--                            </select>--}}
{{--                            @error('type')--}}
{{--                            <div style="display: inline-block;" class="invalid-feedback myval">{{$message}}</div>--}}
{{--                            @enderror--}}

{{--                        </div>--}}
                    </div>
                </div>


                <div class="form-group row mt-5">


                    <!--  Price Input  -->

{{--                    <div class="form-group col-md-6">--}}
{{--                        <label for="inputPrice">قیمت پیشنهادی : </label>--}}
{{--                        <span class="errmsg mr-5" style="font-size: 10pt"></span>--}}
{{--                        <input type="text" name="price" class="form-control inputnumber" id="inputPrice" placeholder="قیمت (تومان) " required value="{{old('price')}}">--}}
{{--                        @error('price')--}}
{{--                        <div style="display: inline-block;" class="invalid-feedback myval">{{$message}}</div>--}}
{{--                        @enderror--}}
{{--                    </div>--}}


                    <div class="form-group col-md-6">
                        <label for="inputTel">شماره تلفن :</label>
                        <span class="errTel mr-5" style="font-size: 10pt"></span>
                        <span class="errmsg mr-5" style="font-size: 10pt"></span>
                        <input type="text" name="phone" class="form-control" id="inputTel" placeholder="5874 574 0910 " required value="{{old('phone')}}">
                        @error('phone')
                        <div style="display: inline-block;" class="invalid-feedback myval">{{$message}}</div>
                        @enderror
                    </div>


                    <div class="container_city_reg_Item col-md-6">

                        <select name="type" class="browser-default custom-select mt-5" style="height: 53%;width: 100%" required>
                            <option @if(old('type')== 'حقیقی و حقوقی') selected @endif value="حقیقی و حقوقی">حقیقی و حقوقی</option>
                            <option @if(old('type')== 'حقیقی') selected @endif value="حقیقی">حقیقی</option>
                            <option @if(old('type')== 'حقوقی') selected @endif value="حقوقی">حقوقی</option>
                        </select>
                        @error('type')
                        <div style="display: inline-block;" class="invalid-feedback myval">{{$message}}</div>
                        @enderror

                    </div>

                </div>


                <!--Captch code-->


                <div class="form-group row text-center mt-5">

                    <div class="container_city_reg_state">

                        <div class="container_city_reg_Item pt-4">
                            <div class="col-sm-12 captcha">
                                {!! captcha_img('math') !!}
                            </div>
                            <span class="btn btn-info refresh"><i class="fa fa-retweet"></i></span>
                        </div>

                        <div class="container_city_reg_Item">
                            <input type="text" name="captcha" placeholder="کد امنیتی" class="w-75 mt-3" id="captcha" required>
                            @error('captcha')
                            <div style="display: inline-block;" class="invalid-feedback myval">{{$message}}</div>
                            @enderror
                        </div>

                    </div>

                </div>

                <div class="form-group row mt-5 error" id="er">
                    کد امنیتی اشتباه است!
                </div>

                <!-- Submit btn -->

                <div class="form-group row mt-5 pt-5">

                    <button type="submit" class="btn btn-info w-50 submit_btn"
                            style="font-size: 13pt ; margin-right: 21% ; padding: 5px">ثبت درخواست
                    </button>

                </div>
            </form>
        </div>

        <div class="flex-Item_con pr-5 dis_im">
            <img src="{{asset('asset/images/img_form.jpg')}}" alt="">
        </div>
    </div>


    <!-- Modal -->
    <div style="direction: ltr" class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header justify-content-center">
                    <span>مدیریت</span>
                </div>
                <div class="modal-body" style="direction: rtl">
                    <div class="alert alert-success" role="alert">
                        آگهی درخواست مجری شما پس از بررسی تا ساعاتی دیگر در سامانه نمایش داده خواهد شد.
                    </div>
                    <div class="alert alert-warning" role="alert">
                        لطفا پس از انجام قرارداد با مجری یا منصرف شدن از آگهی حتما جهت حذف آگهی از طریق باکس پیام در انتهای همین سامانه به مدیریت اطلاع دهید.
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">متوجه شدم</button>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('foot')
    <script src="{{asset('asset/js/bootstrap-validate-2.2.0/dist/bootstrap-validate.js')}}"></script>


    <script>

        var city= $('#city').val()
        if(city != 1)
            $('#state').css('display' , 'none')
        else
            $('#state').css('display' , 'inline-block')





        @if (session('status'))
            $('.modal').modal('show')
        @endif


        $('.modal').on('hide.bs.modal', function (e) {
           location.href='/'
        })

        // $('.submit_btn').click(function (e) {
        //     e.preventDefault()
        //     var v= $('#inputPrice').val()
        //     v= v.replace(/,/gi,'')
        //     $('#inputPrice').val(v)
        //     $('form#adsForm').submit()
        //
        // })

        // $('#inputPrice').keyup(function () {
        //     var x = $(this).val()
        //     x= x.replace(/,/gi,'')
        //     x= parseFloat(x)
        //     if(isNaN(x) == false)
        //     {
        //         var t=x.toLocaleString('ir-IR')
        //         $(this).val(t)
        //     }
        //     else
        //     {
        //         $(this).val('')
        //     }
        //
        // })

        $('.refresh').click(function () {
            $.ajax({
                method: 'get',
                url: '/refreshcaptcha',
                dataType: 'json',
                success: function (msg) {
                    $('.captcha').html(msg.captcha)

                }
            })
        })

        $('#city').change(function () {

            if ($(this).find('option:selected').text().trim()=='تهران')
                $('select#state').fadeIn(200)
            else
                $('select#state').fadeOut(200)
        })

        $('input').on('keydown',function () {
            if ($(this).parent().find('.myval').length)
                $(this).parent().find('.myval').remove()
        })

        bootstrapValidate('#inputName','required:فیلد بالا را پر کنید')
        bootstrapValidate('#inputFamily','required:فیلد بالا را پر کنید')
        bootstrapValidate('#inputceiling','required:فیلد بالا را پر کنید|numeric:مقدار عددی وارد کنید')
        bootstrapValidate('#inputManfi','required:فیلد بالا را پر کنید|numeric:مقدار عددی وارد کنید')
        bootstrapValidate('#inputArea','required:فیلد بالا را پر کنید|numeric:مقدار عددی وارد کنید')
        bootstrapValidate('#inputPrice','required:فیلد بالا را پر کنید')
        bootstrapValidate('#inputTel','required:فیلد بالا را پر کنید|regex:^0[0-9]{10}$: شماره تلفن معتبر وارد کنید ')
        bootstrapValidate('#captcha','required:فیلد بالا را پر کنید')



    </script>
@endsection
