@extends('layout')

@section('content')

    <div style="position: relative;" id="ads" class="printable">
            <div class="jumbotron ">
                <h3 class="display-4">{{$blog->title}}</h3>
                <hr class="my-4">
                <div class="row">
                    <div class="col-md-4 text-center mb-4 img" style="max-height: 262px;max-width: 337px">
                        <img src="{{asset('blog/'.$blog->id.'/blog_'.$blog->id.'.jpg')}}"  alt="">
                    </div>
                    <div class="col-md-8">
                        <div class="lead anjoman" style="max-height: 100% !important;">
                            {!! $blog->text !!}
                        </div>

                    </div>
                </div>
                <hr class="my-4">
            </div>

    </div>


    <div class="row">
        <div class="col-12 ">
            <button class="btn btn-lg btn-secondary" id="print">
                پرینت
                <i class="fa fa-print"></i>
            </button>
            <button class="btn btn-lg btn-info" id="share" data-toggle="modal" data-target="#sharemodal">
                اشتراک گزاری
                <i class="fa fa-share"></i>
            </button>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" style="direction: ltr" id="sharemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="margin: 0; font-weight: 400">اشتراک گزاری</h5>
                </div>
                <div class="modal-body align-center">
                    <a href="tg://msg_url?url=http://www.ejra.ir/news/{{$blog->id}}">
                        <img src="{{asset('asset/images/telegram.png')}}" style="width: 35%" alt="">
                    </a>
                    <a href="whatsapp://send?text=http://www.ejra.ir/news/{{$blog->id}}">
                        <img src="{{asset('asset/images/whatsapp.jpg')}}" style="width: 35%" alt="">
                    </a>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('foot')
    <script src="{{asset('asset/jQuery.print.js')}}"></script>
    <script>


        $('<div style="overflow-x: auto" class="tableparent"></div>').insertAfter('.anjoman table')
        $('.anjoman table').each(function () {
            var afterElement= $(this).next('.tableparent')
            $(this).appendTo(afterElement)
        })

        $('button#print').click(function () {

            $(".printable").print({
                title : 'Ejra'
            });
        })

        $.ajaxSetup({
            headers: {
                'X_CSRF_TOKEN': $('meta[name=csrf-token]').attr('content')
            }
        })

        var pageNumber

        // ads pagination-----------------------------------

        $(document).on('click','.adsPagination a.pgn__num',function(e){
            e.preventDefault()
            var current= $('.adsPagination .current')
            pageNumber= $(this).text()

            current.parent().html('<a class="pgn__num" href="#0">'+current.text()+'</a>')
            $(this).parent().html('<span class="pgn__num current">'+pageNumber+'</span>')
            $('.adsLoading').fadeIn()
            getadvertisement(pageNumber)


        })
        $(document).on('click','.adsPagination a.nxt',function(e){
            e.preventDefault()
            var current= $('.adsPagination .current')
            var target= current.parent().next('li').children('.pgn__num')
            if(target.length)
            {
                pageNumber= target.text()
                current.parent().html('<a class="pgn__num" href="#0">'+current.text()+'</a>')
                target.parent().html('<span class="pgn__num current">'+pageNumber+'</span>')
                $('.adsLoading').fadeIn()
                getadvertisement(pageNumber)

            }

        })
        $(document).on('click','.adsPagination a.prv',function(e){
            e.preventDefault()
            var current= $('.adsPagination .current')
            var target= current.parent().prev('li').children('.pgn__num')
            if(target.length)
            {
                pageNumber= target.text()
                current.parent().html('<a class="pgn__num" href="#0">'+current.text()+'</a>')
                target.parent().html('<span class="pgn__num current">'+pageNumber+'</span>')
                $('.adsLoading').fadeIn()
                getadvertisement(pageNumber)
            }

        })


        function getadvertisement(pagenumber) {
            $.ajax({
                url: '/blogPagination/'+pagenumber,
                method: 'post',
                cache: false,
                data:{showItem:0},
                success: function (msg) {

                    var ads= $('#ads')


                    if(ads.children('.jumbotron').length)
                    {
                        ads.children('.jumbotron').remove()
                    }

                    ads.prepend(msg)



                    $('.loading').fadeOut()
                },
                error: function (msg) {
                    console.log(msg)
                }
            })
        }
    </script>
@endsection
