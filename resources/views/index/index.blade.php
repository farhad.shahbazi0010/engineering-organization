@extends('layout')

@section('content')

    <div class="row">
        <div class="col-12 text-right adsTitle">
            <span>آگهی های درخواست مجری</span>
        </div>
    </div>
    <div class="row" id="ads" style="position: relative">
           @include('index/ads_done_ads/ads')
        <div class="loading adsLoading" style="display: none;position: absolute;top: 0;right: 0;left: 0;bottom: 0;background-color: rgba(0,0,0,.2);">
            <div class="spinner-border " style="position:absolute;;top: 50%;right: 50%;height: 50px;width: 50px"></div>
        </div>
    </div>
   @if($adsCount)
       <div class="row pagination-wrap adsPagination">
           <div class="col-12">
               <div class="col-full">
                   <nav class="pgn">
                       <ul>
                           <li><a class="pgn__prev nxt" href="#0">Prev</a></li>
                           @for($i= 0; $i< ceil($adsCount/6); $i++ )
                               @if($i== 0)
                                   <li><span class="pgn__num current">{{$i+1}}</span></li>
                               @else
                                   <li><a class="pgn__num" href="#0">{{$i+1}}</a></li>
                               @endif
                           @endfor
                           <li><a class="pgn__next prv" href="#0">Next</a></li>
                       </ul>
                   </nav>
               </div>
           </div>
       </div>
   @endif



    <!--ads that done -->
    <div class="row">
        <div class="col-12 text-right adsTitle">
            <span>انجام شده ها</span>
        </div>
    </div>
    <div class="row done" id="done_ads" style="position: relative">
        @include('index/ads_done_ads/done_ads')
        <div class="loading done_adsLoading" style="display: none;position: absolute;top: 0;right: 0;left: 0;bottom: 0;background-color: rgba(0,0,0,.2);">
            <div class="spinner-border " style="position:absolute;;top: 50%;right: 50%;height: 50px;width: 50px"></div>
        </div>
    </div>

     @if($done_adsCount)
         <div class="row pagination-wrap done_adsPagination">
             <div class="col-12">
                 <div class="col-full">
                     <nav class="pgn">
                         <ul>
                             <li><a class="pgn__prev nxt" href="#0">Prev</a></li>
                             @for($i= 0; $i< ceil($done_adsCount/6); $i++ )
                                 @if($i== 0)
                                     <li><span class="pgn__num current">{{$i+1}}</span></li>
                                 @else
                                     <li><a class="pgn__num" href="#0">{{$i+1}}</a></li>
                                 @endif
                             @endfor
                             <li><a class="pgn__next prv" href="#0">Next</a></li>
                         </ul>
                     </nav>
                 </div>
             </div>
         </div>
     @endif

@endsection

@section('foot')

    <script>

        $.ajaxSetup({
            headers: {
                'X_CSRF_TOKEN': $('meta[name=csrf-token]').attr('content')
            }
        })

        function money(){
            @auth()
                var priceSpan= $('#price span.value')
                priceSpan.each(function () {
                    var value= parseFloat($(this).text().replace(/,/gi,''))
                    var formatedMoney= value.toLocaleString()
                    $(this).html(formatedMoney + ' تومان')
                })
            @endauth
        }
        money()

        // this is for resolve a bug that occured in mobile menu
        $(document).on('click','li.has-children',function () {
            var a= $('> a.sub-menu-is-open',this).length
            if (!a)
                $('> a',this).css('color','#e07165')
            else
                $('> a',this).css('color','#4f56a3')
        })

        var pageNumber

        // ads pagination-----------------------------------

        $(document).on('click','.adsPagination a.pgn__num',function(e){
            e.preventDefault()
            var current= $('.adsPagination .current')
            pageNumber= $(this).text()

            current.parent().html('<a class="pgn__num" href="#0">'+current.text()+'</a>')
            $(this).parent().html('<span class="pgn__num current">'+pageNumber+'</span>')
            $('.adsLoading').fadeIn()
            getadvertisement(pageNumber,0)


        })
        $(document).on('click','.adsPagination a.nxt',function(e){
            e.preventDefault()
            var current= $('.adsPagination .current')
            var target= current.parent().next('li').children('.pgn__num')
            if(target.length)
            {
                pageNumber= target.text()
                current.parent().html('<a class="pgn__num" href="#0">'+current.text()+'</a>')
                target.parent().html('<span class="pgn__num current">'+pageNumber+'</span>')
                $('.adsLoading').fadeIn()
                getadvertisement(pageNumber,0)

            }

        })
        $(document).on('click','.adsPagination a.prv',function(e){
            e.preventDefault()
            var current= $('.adsPagination .current')
            var target= current.parent().prev('li').children('.pgn__num')
            if(target.length)
            {
                pageNumber= target.text()
                current.parent().html('<a class="pgn__num" href="#0">'+current.text()+'</a>')
                target.parent().html('<span class="pgn__num current">'+pageNumber+'</span>')
                $('.adsLoading').fadeIn()
                getadvertisement(pageNumber,0)
            }

        })

        // done ads paginaton---------------------

        $(document).on('click','.done_adsPagination a.pgn__num',function(e){
            e.preventDefault()
            var current= $('.done_adsPagination .current')
            pageNumber= $(this).text()

            current.parent().html('<a class="pgn__num" href="#0">'+current.text()+'</a>')
            $(this).parent().html('<span class="pgn__num current">'+pageNumber+'</span>')
            $('.done_adsLoading').fadeIn()
            getadvertisement(pageNumber,1)


        })
        $('.done_adsPagination a.nxt').click(function(e){
            e.preventDefault()
            var current= $('.done_adsPagination .current')
            var target= current.parent().next('li').children('.pgn__num')
            if(target.length)
            {
                pageNumber= target.text()
                current.parent().html('<a class="pgn__num" href="#0">'+current.text()+'</a>')
                target.parent().html('<span class="pgn__num current">'+pageNumber+'</span>')
                $('.done_adsLoading').fadeIn()
                getadvertisement(pageNumber,1)

            }

        })
        $('.done_adsPagination a.prv').click(function(e){
            e.preventDefault()
            var current= $('.done_adsPagination .current')
            var target= current.parent().prev('li').children('.pgn__num')
            if(target.length)
            {
                pageNumber= target.text()
                current.parent().html('<a class="pgn__num" href="#0">'+current.text()+'</a>')
                target.parent().html('<span class="pgn__num current">'+pageNumber+'</span>')
                $('.done_adsLoading').fadeIn()
                getadvertisement(pageNumber,1)
            }

        })



        function getadvertisement(pagenumber,section) {
            $.ajax({
                url: '/adspagination/'+pagenumber+'/'+section,
                method: 'post',
                cache: false,
                data:{showItem:0},
                success: function (msg) {

                    if(section== 0)
                    {
                        var ads= $('#ads')

                        if(ads.children('.col-md-4').length)
                        {
                            ads.children('.col-md-4').remove()
                        }

                        ads.prepend(msg)


                    }
                    else
                    {
                        var done_ads= $('#done_ads')
                        if(done_ads.children('.col-md-4').length)
                        {
                            done_ads.children('.col-md-4').remove()
                        }

                        done_ads.prepend(msg)
                    }
                    money()
                    $('.loading').fadeOut()
                },
                error: function (msg) {
                    console.log(msg)
                }
            })
        }
    </script>
@endsection
