

<footer class="s-footer" style="position: relative;background-image: url({{asset('asset/images/build1.jpg')}}); background-position: center;background-size: cover;background-attachment: fixed">

    <div class="container-fluid" style="z-index: 2; position: relative">
        <div class="s-footer__bottom">
            <div class="row justify-content-center align-items-center">
                <div class="col-md-6">
                    <form action="">
                        <div class="form-group">
                            <label style="color: #fff; font-family: segoe ui; font-weight: 300;" for="">پیام خود را برای ما ارسال کنید</label>
                            <textarea name="messageText" class="form-control" style="min-height: 200px;font-size: 14px;opacity: .7;font-family: Tahoma" placeholder="شماره تماس خود را نیز در پیام درج کنید تا برای پاسخگویی با شما تماس حاصل نماییم"></textarea>
                        </div>
                        <button type="submit" id="sendMessage" class="btn btn-lg btn-primary float-left">ارسال پیام</button>
                    </form>
                </div>
                <div class="col-md-6 fs">
                    <ul class="footer-social">
                        <li>
                            <a href="#0"><i class="fab fa-facebook"></i></a>
                        </li>
                        <li>
                            <a href="#0"><i class="fab fa-instagram"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>



        <!--        <div class="go-top">-->
        <!--            <a class="smoothscroll" title="Back to Top" href="#top"></a>-->
        <!--        </div>-->
    </div>

    <div class="loading sendMessage" style="z-index: 2;display: none;position: absolute;top: 0;right: 0;left: 0;bottom: 0;background-color: rgba(0,0,0,.5);">
        <div class="spinner-border " style="position:absolute;top: 50%;right: 50%;height: 50px;width: 50px"></div>
    </div>

</footer> <!-- end s-footer -->




<script src="{{asset('asset/js/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('asset/bootstrap-4.5.0-dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('asset/js/plugins.js')}}"></script>
<script src="{{asset('asset/js/jquery.toast.js')}}"></script>
<script src="{{asset('asset/js/main.js')}}"></script>
<script>


    $('#sendMessage').click(function (e) {
        e.preventDefault()

        $('div.sendMessage').fadeIn()

        var text= $('textarea[name=messageText]').val()


        $.ajax({
            method: 'post',
            url: '/message/save',
            cache: false,
            data: {text: text, _token:'{{csrf_token()}}'},
            success : function (msg) {
                $('div.sendMessage').fadeOut()


                $.toast({
                    text: 'پیام شما با موفقیت ارسال شد',
                    heading: '',
                    showHideTransition: 'fade',
                    allowToastClose: false,
                    hideAfter: 7000,
                    loader: true,
                    loaderBg: '#fff',
                    stack: 5,
                    position: 'bottom-center',
                    bgColor: false,
                    textColor: false,
                    textAlign: 'right',
                    icon: 'success'

                });

                $('textarea[name=messageText]').val('')

            },
            error: function (msg) {
                console.log(msg)
            }
        })
    })

</script>
@yield('foot')
</body>
</html>
