<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<div>
    <table style="width:80%;min-width: 600px;overflow-x: auto;" cellspacing="0" cellpadding="15" border="1" align="center">
        <tbody>
        <tr>
            <td>a</td>
            <td>b</td>
            <td>c</td>
            <td>d</td>
        </tr>
        <tr>
            <td>e</td>
            <td>f</td>
            <td>g</td>
            <td>h</td>
        </tr>
        <tr>
            <td>i</td>
            <td>g</td>
            <td>k</td>
            <td>l</td>
        </tr>
        <tr>
            <td>m</td>
            <td>n</td>
            <td>o</td>
            <td>p</td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>
