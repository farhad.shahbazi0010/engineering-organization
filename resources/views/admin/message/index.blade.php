@extends('admin/layout')

@section('content')
    <section id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12" >
                    <div style="margin: 13px 0;padding: 18px 0;padding-right: 0px;background-color: #dfc5c5;padding-right: 13px;border-radius: 4px;">
                        <p style="margin: 0; font-weight: bold;color: #000;font-size: 15px">پیام ها</p>

                    </div>
                </div>
            </div>
            <div class="row" id="blogdiv">
                @foreach($messages as $message)
                    <div class="jumbotron" data-id="{{$message->id}}">
                        <h3 class="display-4 jtitle" style="font-size: 14px !important;">
                            {{\Morilog\Jalali\Jalalian::forge($message->created_at)->format('Y/m/d H:i:s')}}
                            @if($message->user_id)
                                <span style="position: absolute;left: 45px;font-size: 14px">
                                    {{$message->user->fullname}}
                                    ردیف {{$message->user_id}}
                                </span>
                            @endif
                        </h3>
                        <hr class="my-4">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="lead">
                                    {{ $message->text }}
                                </div>

                            </div>
                        </div>
                        <hr class="my-4">
                        <div class="text-left">
                            <button class="btn btn-primary btn-sm" id="showAll" href="#" role="button">نمایش کامل</button>
                            <button class="btn btn-danger btn-sm" id="delete" href="#" role="button" style="position: relative">
                                حذف
                                <div id="buttonSpinner">
                                    <div class="spinner-border"></div>
                                </div>
                            </button>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- Modal for add -->
    <div class="modal fade addModal" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">افزودن بلاگ</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="direction: rtl;text-align: right">
                    <form action="#" id="addForm">
                        <div class="form-group">
                            <label for="title" class="" style="font-size: 14px">تیتر</label>
                            <input name="title" type="text" class="form-control col-md-8" id="title" required style="height: 50px;font-size: 15px">
                        </div>
                        <div class="custom-file" style="text-align: left">
                            <input name="file"  type="file" class="custom-file-input" id="validatedCustomFile" required>
                            <label class="custom-file-label col-md-8" for="validatedCustomFile">انتخاب تصویر(jpg.)</label>
                        </div>
                        <div class="form-group mt-2">
                            <label for="text" class="" style="font-size: 14px">متن</label>
                            <textarea  class="form-control" name="ck1"  id="ck1" style="font-size: 15px" required></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="save" style="position: relative">
                        Save changes
                        <div id="buttonSpinner">
                            <div class="spinner-border"></div>
                        </div>
                    </button>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal for show blog full text -->
    <div class="modal fade showModal" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">متن کامل پیام</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="direction: rtl;text-align: right; font-size: 15px;font-family: Tahoma;color: #004c72">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('foot')
    <script>


        $.ajaxSetup({
            headers: {
                'X_CSRF_TOKEN' : $('meta[name=csrf-token]').attr('content')
            }
        })





        $(document).on('click','#delete',function () {

            var _this= $(this)
            Spinner(_this,'on')
            var fi= findId(_this)
            var id= fi[0]
            var jumbotron= fi[1]
            cuteHide(jumbotron)

            $.ajax({
                url: '/admin/message/delete/'+id,
                method: 'post',
                cache: false,
                success: function (msg) {
                    Spinner(_this,'off')
                    if(msg.status== 'ok')
                    {
                        cuteHide(jumbotron)
                    }

                    else
                        alert('مقادیر معتبر ارسال کنید')

                },
                error: function (msg) {

                }
            })
        })


        $(document).on('click','#showAll',function () {
            var _this= $(this)
            var text= _this.parents('.jumbotron').find('div.lead').html()
            $('.showModal').find('.modal-body').html(text)
            $('.showModal').modal('show')
        })

        function Spinner(_this,status) {

            if(status== 'off')
            {
                _this.children('#buttonSpinner').fadeOut()
            }
            if(status== 'on')
            {
                _this.children('#buttonSpinner').fadeIn()
            }
        }

        function findId(_this) {
            var jum= _this.parents('.jumbotron')
            var id= jum.attr('data-id')

            return [id,jum]
        }

        $('input[type=file]').on('change',function(){
            //get the file name
            var fileName = $(this).val();
            //replace the "Choose a file" label
            $(this).next('.custom-file-label').html(fileName);
        })


        function cuteHide(el) {
            el.animate({opacity:'0'},500,function () {
                el.addClass('NoPaddingMargin')
            })
            el.animate({height:'0'},500,function () {
                el.remove()
            })
        }

    </script>
@endsection


