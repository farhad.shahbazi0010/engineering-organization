@extends('admin/layout')

@section('content')
    <section id="main-content" style="margin-right: 210px; font-family: Tahoma">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12" style="margin: 13px 16px;padding: 18px 20px;background-color: #dfc5c5;">
                    <p style="margin: 0; font-weight: bold;color: #000">آگهی های انجام شده</p>
                </div>
            </div>
            <div class="row done" id="done_ads" style="position: relative;">
                @include('index/ads_done_ads/done_ads')
                <div class="loading done_adsLoading" style="display: none;position: absolute;top: 0;right: 0;left: 0;bottom: 0;background-color: rgba(0,0,0,.2);">
                    <div class="spinner-border " style="position:absolute;;top: 50%;right: 50%;height: 50px;width: 50px"></div>
                </div>
            </div>

            <div class="row pagination-wrap done_adsPagination">
                <div class="col-12">
                    <div class="col-full">
                        <nav class="pgn" data-aos="fade-up">
                            <ul>
                                <li><a class="pgn__prev nxt" href="#0">Prev</a></li>
                                @for($i= 0; $i< ceil($done_adsCount/6); $i++ )
                                    @if($i== 0)
                                        <li><span class="pgn__num current">{{$i+1}}</span></li>
                                    @else
                                        <li><a class="pgn__num" href="#0">{{$i+1}}</a></li>
                                    @endif
                                @endfor
                                <li><a class="pgn__next prv" href="#0">Next</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
@section('foot')
    <script>

        $.ajaxSetup({
            headers: {
                'X_CSRF_TOKEN' : $('meta[name=csrf-token]').attr('content')
            }
        })
        $(document).on('click','#delete_done_ads',function () {
            var _this= $(this)
            var id= _this.parents('.card').attr('data-id')
            Spinner(_this,'on')

            var el= _this.parents('.col-md-4')


            $.ajax({
                url: '/admin/ads/delete/'+id,
                method: 'post',
                success: function (msg) {
                    Spinner(_this,'off')
                    cuteHide(el)
                },
                error: function (msg) {

                }
            })
        })
        $(document).on('click','#change_done_ads',function () {
            var _this= $(this)
            var id= _this.parents('.card').attr('data-id')
            Spinner(_this,'on')

            $.ajax({
                url: '/admin/change_done_ads/'+id,
                method: 'post',
                success: function (msg) {
                    Spinner(_this,'off')
                    var temp = _this.parents('.col-md-4').remove()
                },
                error: function (msg) {

                }
            })
        })

        function Spinner(_this,status) {

            if(status== 'off')
            {
                _this.children('#buttonSpinner').fadeOut()
            }
            if(status== 'on')
            {
                _this.children('#buttonSpinner').fadeIn()
            }
        }
        function cuteHide(el) {
            el.animate({opacity:'0'},500,function () {
                el.addClass('NoPaddingMargin')
            })
            el.animate({'max-width':'0%'},1000,function () {
                el.remove()
            })
        }

        var pageNumber

        // done ads paginaton---------------------

        $(document).on('click','.done_adsPagination a.pgn__num',function(e){
            e.preventDefault()
            var current= $('.done_adsPagination .current')
            pageNumber= $(this).text()

            current.parent().html('<a class="pgn__num" href="#0">'+current.text()+'</a>')
            $(this).parent().html('<span class="pgn__num current">'+pageNumber+'</span>')
            $('.done_adsLoading').fadeIn()
            getadvertisement(pageNumber,1)


        })
        $('.done_adsPagination a.nxt').click(function(e){
            e.preventDefault()
            var current= $('.done_adsPagination .current')
            var target= current.parent().next('li').children('.pgn__num')
            if(target.length)
            {
                pageNumber= target.text()
                current.parent().html('<a class="pgn__num" href="#0">'+current.text()+'</a>')
                target.parent().html('<span class="pgn__num current">'+pageNumber+'</span>')
                $('.done_adsLoading').fadeIn()
                getadvertisement(pageNumber,1)

            }

        })
        $('.done_adsPagination a.prv').click(function(e){
            e.preventDefault()
            var current= $('.done_adsPagination .current')
            var target= current.parent().prev('li').children('.pgn__num')
            if(target.length)
            {
                pageNumber= target.text()
                current.parent().html('<a class="pgn__num" href="#0">'+current.text()+'</a>')
                target.parent().html('<span class="pgn__num current">'+pageNumber+'</span>')
                $('.done_adsLoading').fadeIn()
                getadvertisement(pageNumber,1)
            }

        })


        function getadvertisement(pagenumber,section) {
            $.ajax({
                url: '/adspagination/'+pagenumber+'/'+section,
                method: 'post',
                cache: false,
                data:{showItem:1},
                success: function (msg) {

                    if(section== 0)
                    {
                        var ads= $('#ads')

                        if(ads.children('.col-md-4').length)
                        {
                            ads.children('.col-md-4').remove()

                        }

                        ads.prepend(msg)


                    }
                    else
                    {
                        var done_ads= $('#done_ads')
                        if(done_ads.children('.col-md-4').length)
                        {
                            done_ads.children('.col-md-4').remove()
                        }

                        done_ads.prepend(msg)
                    }
                    $('.loading').fadeOut()
                },
                error: function (msg) {
                    console.log(msg)
                }
            })
        }

    </script>
@endsection

