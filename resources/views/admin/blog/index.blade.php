@extends('admin/layout')

@section('content')
    <section id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12" >
                    <div style="margin: 13px 0;padding: 18px 0;padding-right: 0px;background-color: #dfc5c5;padding-right: 13px;border-radius: 4px;">
                        <p style="margin: 0; font-weight: bold;color: #000;font-size: 15px">اخبار و اطلاعیه ها</p>
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModalCenter" style="position:absolute;left: 26px;top: 28px;font-size: 11px">
                            افزودن
                            <div id="buttonSpinner">
                                <div class="spinner-border"></div>
                            </div>
                        </button>

                    </div>
                </div>
            </div>
            <div class="row" id="blogdiv" style="position: relative">
                @foreach($blogs as $blog)
                    <div class="jumbotron" data-id="{{$blog->id}}">
                        <h3 class="display-4 jtitle">{{$blog->title}}</h3>
                        <hr class="my-4">
                        <div class="row">
                            <div class="col-md-4 text-center mb-4 img" style="max-height: 262px;max-width: 337px">
                                <img src="{{asset('blog/'.$blog->id.'/blog_'.$blog->id.'.jpg')}}"  alt="">
                            </div>
                            <div class="col-md-8">
                                <div class="lead">
                                    {!! $blog->text !!}
                                </div>

                            </div>
                        </div>
                        <hr class="my-4">
                        <div class="text-left">
                            <button class="btn btn-primary btn-sm" id="showAll" href="#" role="button">نمایش کامل</button>
                            <button class="btn btn-info btn-sm" id="edit" href="#" role="button">ویرایش</button>
                            <button class="btn btn-danger btn-sm" id="delete" href="#" role="button" style="position: relative">
                                حذف
                                <div id="buttonSpinner">
                                    <div class="spinner-border"></div>
                                </div>
                            </button>
                        </div>
                    </div>
                @endforeach

                    <div class="loading adsLoading" style="display: none;position: absolute;top: 0;right: 0;left: 0;bottom: 0;background-color: rgba(0,0,0,.2);">
                        <div class="spinner-border " style="position:absolute;top: 93%;right: 50%;height: 50px;width: 50px"></div>
                    </div>

            </div>
            @if($count)
                <div class="row pagination-wrap adsPagination">
                    <div class="col-12">
                        <div class="col-full">
                            <nav class="pgn" data-aos="fade-up">
                                <ul>
                                    <li><a class="pgn__prev nxt" href="#0">Prev</a></li>
                                    @for($i= 0; $i< ceil($count/6); $i++ )
                                        @if($i== 0)
                                            <li><span class="pgn__num current">{{$i+1}}</span></li>
                                        @else
                                            <li><a class="pgn__num" href="#0">{{$i+1}}</a></li>
                                        @endif
                                    @endfor
                                    <li><a class="pgn__next prv" href="#0">Next</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>

            @endif

        </div>
    </section>
    <!-- Modal for add -->

    <div class="modal fade addModal" id="exampleModalCenter" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">افزودن بلاگ</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="direction: rtl;text-align: right">
                    <form action="#" id="addForm">
                        <div class="form-group">
                            <label for="title" class="" style="font-size: 14px">تیتر</label>
                            <input name="title" type="text" class="form-control col-md-8" id="title" required style="height: 50px;font-size: 15px">
                        </div>
                        <div class="custom-file" style="text-align: left">
                            <input name="file"  type="file" class="custom-file-input" id="validatedCustomFile" required>
                            <label class="custom-file-label col-md-8" for="validatedCustomFile">انتخاب تصویر(jpg.)</label>
                        </div>
                        <div class="form-group mt-2">
                            <label for="text" class="" style="font-size: 14px">متن</label>
                            <textarea  class="form-control" name="ck1"  id="ck1" style="font-size: 15px" required></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="save" style="position: relative">
                        Save changes
                        <div id="buttonSpinner">
                            <div class="spinner-border"></div>
                        </div>
                    </button>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal for edit -->
    <div class="modal fade editModal"  role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">ویرایش بلاگ</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="direction: rtl;text-align: right">
                    <form action="#" id="editForm">
                        <div class="form-group">
                            <label for="title" class="" style="font-size: 14px">تیتر</label>
                            <input name="title" type="text" class="form-control col-md-8" id="title" required style="height: 50px;font-size: 15px">
                        </div>
                        <div class="custom-file" style="text-align: left">
                            <input name="file"  type="file" class="custom-file-input" id="validatedCustomFile" required>
                            <label class="custom-file-label col-md-8" for="validatedCustomFile">انتخاب تصویر(jpg.)</label>
                        </div>
                        <div class="form-group mt-2">
                            <label for="text" class="" style="font-size: 14px">متن</label>
                            <textarea  class="form-control" name="ck2"  id="ck2" style="font-size: 15px" required></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="update" style="position: relative">
                        Update
                        <div id="buttonSpinner">
                            <div class="spinner-border"></div>
                        </div>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal for show blog full text -->
    <div class="modal fade showModal" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">متن کامل بلاگ</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="direction: rtl;text-align: right; font-size: 15px;font-family: Tahoma;color: #004c72">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('foot')
    <script src="{{asset('asset/ckeditor/ckeditor.js')}}"></script>

    <script>


        var ck1= CKEDITOR.replace( 'ck1' );
        var ck2= CKEDITOR.replace( 'ck2' );

        $(".modal").removeAttr("tabindex");

        $.ajaxSetup({
            headers: {
                'X_CSRF_TOKEN' : $('meta[name=csrf-token]').attr('content')
            }
        })



        $(document).on('click','#save',function () {
            var _this= $(this)
            Spinner(_this,'on')

            var text= CKEDITOR.instances.ck1.getData()


            var formdata= new FormData($('#addForm')[0])
            formdata.append('text',text)


            $.ajax({
                url: '/admin/blog/save',
                method: 'post',
                data:formdata,
                processData: false,
                contentType : false,
                cache: false,
                success: function (msg) {
                    Spinner(_this,'off')
                    if(msg.status== 'ok')
                    {
                        $('.addModal').modal('hide')
                        $('#addForm')[0].reset()
                        $('.custom-file-label').html('انتخاب تصویر(jpg.)')
                        CKEDITOR.instances.ck1.setData('')
                        addToBlog(msg.id,formdata.get('title'),formdata.get('text'))
                    }

                    else
                        alert('فیلدهای الزامی را پر کنید')

                },
                error: function (msg) {
                    console.log(msg)
                }
            })
        })
        var jumbotron,
            id
        $(document).on('click','#edit',function () {
            var _this= $(this)
            var fi= findId(_this)
            id= fi[0]
            jumbotron= fi[1]

            var title= jumbotron.find('.jtitle').html(),
                text= jumbotron.find('div.lead').html()


            $('#editForm').find('input[name=title]').val(title)
            CKEDITOR.instances.ck2.setData(text)



            $('.editModal').modal('show')
        })

        $(document).on('click','#update',function () {
            var _this= $(this)
            Spinner(_this,'on')

            var text= CKEDITOR.instances.ck2.getData()

            var formdata= new FormData($('#editForm')[0])
            formdata.append('text',text)




            $.ajax({
                url: '/admin/blog/update/'+id,
                method: 'post',
                data:formdata,
                processData: false,
                contentType: false,
                cache: false,
                success: function (msg) {
                    Spinner(_this,'off')
                    if(msg.status== 'ok')
                    {
                        jumbotron.find('.jtitle').html(formdata.get('title'))
                        jumbotron.find('.lead').html(formdata.get('text'))

                        var date= new Date()
                        jumbotron.find('.img img').attr('src','/blog/'+id+'/blog_'+id+'.jpg?t='+date.getTime()+'')
                        $('.editModal').modal('hide')

                    }

                    else
                        alert('فیلد های الزامی را پر کنید')

                },
                error: function (msg) {

                }
            })
        })


        $(document).on('click','#delete',function () {

            var _this= $(this)
            Spinner(_this,'on')
            var fi= findId(_this)
            var id= fi[0]
            var jumbotron= fi[1]

            $.ajax({
                url: '/admin/blog/delete/'+id,
                method: 'post',
                cache: false,
                success: function (msg) {
                    console.log(msg)
                    Spinner(_this,'off')
                    if(msg.status== 'ok')
                    {
                        cuteHide(jumbotron)
                    }

                    else
                        alert('مقادیر معتبر ارسال کنید')

                },
                error: function (msg) {
                    console.log(msg)
                }
            })
        })


        $(document).on('click','#showAll',function () {
            var _this= $(this)
            var text= _this.parents('.jumbotron').find('div.lead').html()
            $('.showModal').find('.modal-body').html(text)
            $('.showModal').modal('show')
        })

        function Spinner(_this,status) {

            if(status== 'off')
            {
                _this.children('#buttonSpinner').fadeOut()
            }
            if(status== 'on')
            {
                _this.children('#buttonSpinner').fadeIn()
            }
        }

        function findId(_this) {
            var jum= _this.parents('.jumbotron')
            var id= jum.attr('data-id')

            return [id,jum]
        }

        $('input[type=file]').on('change',function(){
            //get the file name
            var fileName = $(this).val();
            //replace the "Choose a file" label
            $(this).next('.custom-file-label').html(fileName);
        })

        function addToBlog(id,title,text) {
            $('#blogdiv').prepend(' <div class="jumbotron" data-id="'+id+'">\n' +
                '                        <h3 class="display-4 jtitle">'+title+'</h3>\n' +
                '                        <hr class="my-4">\n' +
                '                        <div class="row">\n' +
                '                            <div class="col-md-4 text-center mb-4 img" style="max-height: 262px;max-width: 337px">\n' +
                '                                <img src="/blog/'+id+'/blog_'+id+'.jpg"  alt="">\n' +
                '                            </div>\n' +
                '                            <div class="col-md-8">\n' +
                '                                <div class="lead">\n' +
                '                                    '+text+'\n' +
                '                                </div>\n' +
                '\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                        <hr class="my-4">\n' +
                '                        <div class="text-left">\n' +
                '                            <button class="btn btn-primary btn-sm" id="showAll" href="#" role="button">نمایش کامل</button>\n' +
                '                            <button class="btn btn-info btn-sm" id="edit" href="#" role="button">ویرایش</button>\n' +
                '                            <button class="btn btn-danger btn-sm" id="delete" href="#" role="button">حذف</button>\n' +
                '                        </div>\n' +
                '                    </div>')
        }

        function cuteHide(el) {
            el.animate({opacity:'0'},500,function () {
                el.addClass('NoPaddingMargin')
            })
            el.animate({height:'0'},500,function () {
                el.remove()
            })
        }

        var pageNumber

        // ads pagination-----------------------------------

        $(document).on('click','.adsPagination a.pgn__num',function(e){
            e.preventDefault()
            var current= $('.adsPagination .current')
            pageNumber= $(this).text()

            current.parent().html('<a class="pgn__num" href="#0">'+current.text()+'</a>')
            $(this).parent().html('<span class="pgn__num current">'+pageNumber+'</span>')
            $('.adsLoading').fadeIn()
            getadvertisement(pageNumber,0)


        })
        $(document).on('click','.adsPagination a.nxt',function(e){
            e.preventDefault()
            var current= $('.adsPagination .current')
            var target= current.parent().next('li').children('.pgn__num')
            if(target.length)
            {
                pageNumber= target.text()
                current.parent().html('<a class="pgn__num" href="#0">'+current.text()+'</a>')
                target.parent().html('<span class="pgn__num current">'+pageNumber+'</span>')
                $('.adsLoading').fadeIn()
                getadvertisement(pageNumber,0)

            }

        })
        $(document).on('click','.adsPagination a.prv',function(e){
            e.preventDefault()
            var current= $('.adsPagination .current')
            var target= current.parent().prev('li').children('.pgn__num')
            if(target.length)
            {
                pageNumber= target.text()
                current.parent().html('<a class="pgn__num" href="#0">'+current.text()+'</a>')
                target.parent().html('<span class="pgn__num current">'+pageNumber+'</span>')
                $('.adsLoading').fadeIn()
                getadvertisement(pageNumber,0)
            }

        })

        function getadvertisement(pagenumber) {
            $.ajax({
                url: '/blogPagination/'+pagenumber,
                method: 'post',
                cache: false,
                data:{showItem:1},
                success: function (msg) {

                    var ads= $('#blogdiv')


                    if(ads.children('.jumbotron').length)
                    {
                        ads.children('.jumbotron').remove()
                    }

                    ads.prepend(msg)



                    $('.loading').fadeOut()
                },
                error: function (msg) {
                    console.log(msg)
                }
            })
        }

    </script>
@endsection


