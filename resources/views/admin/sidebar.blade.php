<aside id="aside">
    <div id="sidebar" class="nav-collapse" style="min-height: 512px">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
            <p class="centered"><a href="/"><img src="{{asset('asset/images/images-2.jpeg')}}" class="img-circle" width="80"></a></p>
            <h5 class="centered">EJ RA</h5>
            <li class="sub-menu">
                <a href="javascript:;">
                    <i class="fa fa-desktop"></i>
                    <span>مدیریت آگهی ها</span>
                </a>
                <ul class="sub">
                    <li><a href="/admin/ads">آگهی ها</a></li>
                    <li><a href="/admin/done_ads">آگهی های انجام شده</a></li>
                </ul>
            </li>
            <li class="sub-menu">
                <a href="javascript:;">
                    <i class="fa fa-user"></i>
                    <span>مدیریت کاربران</span>
                </a>
                <ul class="sub">
                    <li><a href="/admin/user"> کاربران اجرا </a></li>
                    <li><a href="/admin/manager">مدیران اجرا</a></li>
                </ul>
            </li>
            <li>
                <a href="/admin/blog">
                    <i class="fa fa-newspaper"></i>
                    <span>اخبار و اطلاعیه ها</span>
                </a>
            </li>
            <li>
                <a href="/admin/message">
                    <i class="fa fa-envelope"></i>
                    <span>پیام ها</span>
                </a>
            </li>
            <li>
                <a href="/admin/association">
                    <i class="fa fa-cogs"></i>
                    <span>مدیریت تعرفه</span>
                </a>
            </li>

        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
