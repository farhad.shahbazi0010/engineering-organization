<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
{{--    <meta name="viewport" content="width=device-width, initial-scale=1.0">--}}
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, minimum-scale=1.0">
    <meta name="description" content="سامانه آنلاین خدمات مهندسی ساختمان">
{{--    <meta name="author" content="Dashboard">--}}
    <meta name="csrf-token" content="{{csrf_token()}}">
{{--    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">--}}
    <title>سامانه آنلاین خدمات مهندسی ساختمان</title>

    <!-- Favicons -->

{{--    <link href="{{asset('asset/dashio/img/apple-touch-icon.png')}}" rel="apple-touch-icon">--}}

    <!-- Bootstrap core CSS -->
    <link href="{{asset('asset/bootstrap-4.5.0-dist/css/bootstrap.css')}}" rel="stylesheet">
    <!--external css-->
    <link href="{{asset('asset/css/font-awesome/css/fontawesome-all.min.css')}}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{asset('asset/dashio/css/zabuto_calendar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('asset/dashio/lib/gritter/css/jquery.gritter.css')}}" />
    <!-- Custom styles for this template -->
    <link href="{{asset('asset/css/jquery.toast.css')}}" rel="stylesheet">

    <link href="{{asset('asset/dashio/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('asset/dashio/css/style-responsive.css')}}" rel="stylesheet">
    <script src="{{asset('asset/dashio/lib/chart-master/Chart.js')}}"></script>


    <!-- =======================================================
      Template Name: Dashio
      Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
      Author: TemplateMag.com
      License: https://templatemag.com/license/
    ======================================================= -->

</head>

<body>

@php
    $mCount = App\Message::where('is_read',0)->count();
@endphp

<section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
        <div class="sidebar-toggle-box">
            <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
        </div>
        <!--logo start-->
        <a href="/" class="logo"><b>EJ<span>RA</span></b></a>
        <!--logo end-->
        <div class="nav notify-row" id="top_menu">

        </div>
        <div class="top-menu">
            <ul class="nav top-menu mt-3" style="float: right">

                <li style="position: relative;margin-right: 14px;bottom: 4px;">
                    <i class="fa fa-envelope" style="color:#fff;font-size: 28px"></i>
                   <span style="position: absolute;background-color: orange;border-radius: 50%;left: -15px;top: -7px;width: 23px;height: 23px;text-align: center;font-size: 17px;font-weight: bold;">{{$mCount}}</span>
                </li>

                <li><a class="logout" href="#" onclick="
document.getElementById('logoutForm').submit()">Logout</a></li>
            </ul>
        </div>
        <form id="logoutForm" action="{{route('logout')}}" method="post">@csrf</form>
    </header>
