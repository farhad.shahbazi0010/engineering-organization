@extends('admin/layout')

@section('content')
    <section id="main-content" style="margin-right: 210px; font-family: Tahoma">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12" >
                    <div style="margin: 13px 0;padding: 18px 0;padding-right: 0px;background-color: #dfc5c5;padding-right: 13px;border-radius: 4px;">
                        <p style="margin: 0; font-weight: bold;color: #000;font-size: 15px">آگهی ها</p>
                        <a href="/export/1" class="btn btn-info" style="position: absolute; left: 26px; top: 28px;font-size: 11px">خروجی اکسل</a>
                    </div>
                </div>
            </div>
            <div class="row" id="ads" style="position:relative;">
               @include('index/ads_done_ads/ads')
                <div class="loading adsLoading" style="display: none;position: absolute;top: 0;right: 0;left: 0;bottom: 0;background-color: rgba(0,0,0,.2);">
                    <div class="spinner-border " style="position:absolute;;top: 50%;right: 50%;height: 50px;width: 50px"></div>
                </div>
            </div>

            <div class="row pagination-wrap adsPagination">
                <div class="col-12">
                    <div class="col-full">
                        <nav class="pgn" data-aos="fade-up">
                            <ul>
                                <li><a class="pgn__prev nxt" href="#0">Prev</a></li>
                                        @for($i= 0; $i< ceil($adsCount/6); $i++ )
                                            @if($i== 0)
                                                <li><span class="pgn__num current">{{$i+1}}</span></li>
                                            @else
                                                <li><a class="pgn__num" href="#0">{{$i+1}}</a></li>
                                            @endif
                                        @endfor
                                <li><a class="pgn__next prv" href="#0">Next</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
@section('foot')
    <script>



        $.toast({
            text: 'به سامانه مدیریت اجرا خوش آمدید',
            heading: '',
            showHideTransition: 'fade',
            allowToastClose: true,
            hideAfter: 5000,
            loader: true,
            loaderBg: '#fff',
            stack: 5,
            position: 'top-center',
            bgColor: false,
            textColor: false,
            textAlign: 'left',
            icon: 'info'

        });


        $.ajaxSetup({
            headers: {
                'X_CSRF_TOKEN' : $('meta[name=csrf-token]').attr('content')
            }
        })
        $(document).on('click','#deleteAds',function () {
            var _this= $(this)
            var id= _this.parents('.card').attr('data-id')
            Spinner(_this,'on')
            var el= _this.parents('.col-md-4')


            $.ajax({
                url: '/admin/ads/delete/'+id,
                method: 'post',
                success: function (msg) {
                    Spinner(_this,'off')
                    cuteHide(el)
                },
                error: function (msg) {

                }
            })
        })

        function cuteHide(el) {
            el.animate({opacity:'0'},500,function () {
                el.addClass('NoPaddingMargin')
            })
            el.animate({'max-width':'0%'},1000,function () {
                el.remove()
            })
        }
        $(document).on('click','#changeAds',function () {
            var _this= $(this)
            var id= _this.parents('.card').attr('data-id')
            Spinner(_this,'on')

            $.ajax({
                url: '/admin/changeAds/'+id,
                method: 'post',
                success: function (msg) {
                    Spinner(_this,'off')
                    var temp = _this.parents('.col-md-4').remove()
                },
                error: function (msg) {

                }
            })
        })
        $(document).on('click','#approval',function () {
            var _this= $(this)
            var _parent= _this.parent()
            var id= _this.parents('.card').attr('data-id')
            Spinner(_this,'on')

            $.ajax({
                url: '/admin/ads_approval/'+id,
                method: 'post',
                success: function (msg) {
                    Spinner(_this,'off')
                    _this.remove()
                    _parent.append('  <button id="disapproval" class="btn btn-warning" style="position:relative;">\n' +
                        '                                        تایید نمیکنم\n' +
                        '                                        <div id="buttonSpinner">\n' +
                        '                                            <div class="spinner-border"></div>\n' +
                        '                                        </div>\n' +
                        '                                    </button>')
                },
                error: function (msg) {
                    console.log(msg)
                }
            })
        })
        $(document).on('click','#disapproval',function () {
            var _this= $(this)
            var _parent= _this.parent()
            var id= _this.parents('.card').attr('data-id')
            Spinner(_this,'on')

            $.ajax({
                url: '/admin/ads_disapproval/'+id,
                method: 'post',
                success: function (msg) {
                    Spinner(_this,'off')
                    _this.remove()
                    _parent.append('  <button id="approval" class="btn btn-success" style="position:relative;">\n' +
                        '                                        تایید میکنم\n' +
                        '                                        <div id="buttonSpinner">\n' +
                        '                                            <div class="spinner-border"></div>\n' +
                        '                                        </div>\n' +
                        '                                    </button>')
                },
                error: function (msg) {
                    console.log(msg)
                }
            })
        })

        function Spinner(_this,status) {

            if(status== 'off')
            {
                _this.children('#buttonSpinner').fadeOut()
            }
            if(status== 'on')
            {
                _this.children('#buttonSpinner').fadeIn()
            }
        }



        var pageNumber

        // ads pagination-----------------------------------

        $(document).on('click','.adsPagination a.pgn__num',function(e){
            e.preventDefault()
            var current= $('.adsPagination .current')
            pageNumber= $(this).text()

            current.parent().html('<a class="pgn__num" href="#0">'+current.text()+'</a>')
            $(this).parent().html('<span class="pgn__num current">'+pageNumber+'</span>')
            $('.adsLoading').fadeIn()
            getadvertisement(pageNumber,0)


        })
        $(document).on('click','.adsPagination a.nxt',function(e){
            e.preventDefault()
            var current= $('.adsPagination .current')
            var target= current.parent().next('li').children('.pgn__num')
            if(target.length)
            {
                pageNumber= target.text()
                current.parent().html('<a class="pgn__num" href="#0">'+current.text()+'</a>')
                target.parent().html('<span class="pgn__num current">'+pageNumber+'</span>')
                $('.adsLoading').fadeIn()
                getadvertisement(pageNumber,0)

            }

        })
        $(document).on('click','.adsPagination a.prv',function(e){
            e.preventDefault()
            var current= $('.adsPagination .current')
            var target= current.parent().prev('li').children('.pgn__num')
            if(target.length)
            {
                pageNumber= target.text()
                current.parent().html('<a class="pgn__num" href="#0">'+current.text()+'</a>')
                target.parent().html('<span class="pgn__num current">'+pageNumber+'</span>')
                $('.adsLoading').fadeIn()
                getadvertisement(pageNumber,0)
            }

        })


        function getadvertisement(pagenumber,section) {
            $.ajax({
                url: '/adspagination/'+pagenumber+'/'+section,
                method: 'post',
                cache: false,
                data:{showItem:1},
                success: function (msg) {

                    if(section== 0)
                    {
                        var ads= $('#ads')

                        if(ads.children('.col-md-4').length)
                        {
                            ads.children('.col-md-4').remove()

                        }

                        ads.prepend(msg)


                    }
                    else
                    {
                        var done_ads= $('#done_ads')
                        if(done_ads.children('.col-md-4').length)
                        {
                            done_ads.children('.col-md-4').remove()
                        }

                        done_ads.prepend(msg)
                    }
                    $('.loading').fadeOut()
                },
                error: function (msg) {
                    console.log(msg)
                }
            })
        }


    </script>
@endsection
