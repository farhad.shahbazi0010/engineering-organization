
<!--main content end-->
<!--footer start-->

<!--footer end-->
</section>
<!-- js placed at the end of the document so the pages load faster -->
<script src="{{asset('asset/dashio/lib/jquery/jquery.min.js')}}"></script>

<script src="{{asset('asset/bootstrap-4.5.0-dist/js/bootstrap.js')}}"></script>
<script class="include" type="text/javascript" src="{{asset('asset/dashio/lib/jquery.dcjqaccordion.2.7.js')}}"></script>
<script src="{{asset('asset/dashio/lib/jquery.scrollTo.min.js')}}"></script>
<script src="{{asset('asset/dashio/lib/jquery.nicescroll.js')}}" type="text/javascript"></script>
<script src="{{asset('asset/dashio/lib/jquery.sparkline.js')}}"></script>
<!--common script for all pages-->
<script src="{{asset('asset/dashio/lib/common-scripts.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/dashio/lib/gritter/js/jquery.gritter.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/dashio/lib/gritter-conf.js')}}"></script>
<!--script for this page-->
{{--<script src="{{asset('asset/dashio/lib/sparkline-chart.js')}}"></script>--}}
{{--<script src="{{asset('asset/dashio/lib/zabuto_calendar.js')}}"></script>--}}
<script src="{{asset('asset/js/jquery.toast.js')}}"></script>
@yield('foot')


</body>

</html>
