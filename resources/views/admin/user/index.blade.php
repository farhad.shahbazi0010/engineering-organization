@extends('admin/layout')

@section('content')
    <section id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12" >
                    <div style="margin: 13px 0;padding: 18px 0;padding-right: 0px;background-color: #dfc5c5;padding-right: 13px;border-radius: 4px;">
                        <p style="margin: 0; font-weight: bold;color: #000;font-size: 15px">کاربران</p>
                        <button id="delete" class="btn btn-danger float-left" style="position:absolute;left: 26px;top: 28px;font-size: 11px">
                            حذف
                            <div id="buttonSpinner">
                                <div class="spinner-border"></div>
                            </div>
                        </button>
                        <button id="changeAdmin" class="btn btn-primary float-left" style="position:absolute;left: 79px;top: 28px;font-size: 11px">
                            تغییر به مدیر
                            <div id="buttonSpinner">
                                <div class="spinner-border"></div>
                            </div>
                        </button>

                        <a href="/export/2" class="btn btn-info" style="position: absolute; left: 168px; top: 28px;font-size: 11px">خروجی اکسل</a>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div style="overflow: auto" id="ss">
                        <table class="table" style="min-width: 800px;z-index: -100">
                            <thead>
                            <tr class="table-active">
                                <th scope="col">ردیف</th>
                                <th scope="col">نام و نام خانوادگی</th>
                                <th scope="col">شماره تلفن</th>
                                <th scope="col">نقش</th>
                                <th scope="col">تاریخ عضویت</th>
                                <th scope="col">انتخاب</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <th class="id">{{$user->id}}</th>
                                    <td>{{$user->fullname}}</td>
                                    <td>{{$user->phone}}</td>
                                    <td>{{$user->role}}</td>
                                    <td>{{\Morilog\Jalali\Jalalian::forge($user->created_at)->format('Y/m/d')}}</td>
                                    <td>
                                        <div class="custom-control custom-checkbox mr-sm-2">
                                            <input type="checkbox" class="custom-control-input" id="checkbox{{$user->id}}">
                                            <label class="custom-control-label" for="checkbox{{$user->id}}"></label>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('foot')
    <script>
        uncheckedAll()

        $.ajaxSetup({
            headers: {
                'X_CSRF_TOKEN' : $('meta[name=csrf-token]').attr('content')
            }
        })

        var checkbox
        var tr

        $(document).on('click','#delete',function () {
            var _this= $(this)
            Spinner(_this,'on')
            var ids= findId()


            $.ajax({
                url: '/admin/user/delete',
                method: 'post',
                data:{ids:ids},
                success: function (msg) {
                    Spinner(_this,'off')
                    if(msg.status== 'ok')
                        tr.remove()
                    else
                        alert('مقداری انتخاب نشده است')

                },
                error: function (msg) {

                }
            })
        })
        $(document).on('click','#changeAdmin',function () {
            var _this= $(this)
            Spinner(_this,'on')
            var ids= findId()

            $.ajax({
                url: '/admin/changeToAdmin',
                method: 'post',
                data:{ids:ids,change: 1},
                success: function (msg) {
                    Spinner(_this,'off')
                    if(msg.status== 'ok')
                        tr.remove()
                    else
                        alert('مقداری انتخاب نشده است')

                },
                error: function (msg) {

                }
            })
        })

        function Spinner(_this,status) {

            if(status== 'off')
            {
                _this.children('#buttonSpinner').fadeOut()
            }
            if(status== 'on')
            {
                _this.children('#buttonSpinner').fadeIn()
            }
        }

        function findId() {
             var IDS= []
             checkbox= $('tbody').find('input[type=checkbox]:checked')
             tr= checkbox.parents('tr')

             tr.each(function (index,value) {
                 IDS.push($(this).children('.id').text())
             })
            return IDS
        }

        function uncheckedAll() {
            $('tbody').find('input[type=checkbox]').prop('checked',false)
        }



    </script>
@endsection

