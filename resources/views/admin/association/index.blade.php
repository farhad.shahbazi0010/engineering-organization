@extends('admin/layout')

@section('content')
    <section id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12" >
                    <div style="margin: 13px 0;padding: 18px 0;padding-right: 0px;background-color: #dfc5c5;padding-right: 13px;border-radius: 4px;">
                        <p style="margin: 0; font-weight: bold;color: #000;font-size: 15px">تعرفه</p>

                    </div>
                </div>
            </div>
            <div class="row" id="blogdiv">

                    <div class="col-md-6 text-right adsTitle mb-5">
                        <form action="" id="editForm">
                            <div class="form-group">
                                <label for="">عنوان</label>
                                <input type="text" name="title" class="form-control" style="height: 40px;font-size: 15px" value="{{$asso->title}}">
                            </div>
{{--                            <div class="custom-file" style="text-align: left">--}}
{{--                                <input name="file"  type="file" class="custom-file-input" id="validatedCustomFile" required>--}}
{{--                                <label class="custom-file-label col-md-8" for="validatedCustomFile">انتخاب تصویر(jpg.)</label>--}}
{{--                            </div>--}}
                            <button id="update" type="button" class="btn btn-info btn-sm mt-3" style="position: relative">
                                ویرایش
                                <div id="buttonSpinner">
                                    <div class="spinner-border"></div>
                                </div>
                            </button>
                        </form>
                    </div>


                <div class="col-12 text-center asso mb-5" >
{{--                    <img src="{{asset('associationfolder/association.jpg')}}" alt="">--}}
                    <textarea id="ck">{{$asso->text}}</textarea>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal for show blog full text -->
    <div class="modal fade showModal" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">متن کامل پیام</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="direction: rtl;text-align: right; font-size: 15px;font-family: Tahoma;color: #004c72">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('foot')
    <script src="{{asset('asset/ckeditor/ckeditor.js')}}"></script>
    <script>


        var ck= CKEDITOR.replace('ck')

        $.ajaxSetup({
            headers: {
                'X_CSRF_TOKEN' : $('meta[name=csrf-token]').attr('content')
            }
        })





        $(document).on('click','#update',function () {


            var text= CKEDITOR.instances.ck.getData()
            console.log(text)
            var _this= $(this)
            Spinner(_this,'on')
           var formdata= new FormData($('#editForm')[0])
               formdata.append('text',text)
            $.ajax({
                url: '/admin/association/update',
                method: 'post',
                data: formdata,
                cache: false,
                processData:false,
                contentType: false,
                success: function (msg) {
                    Spinner(_this,'off')
                    if(msg.status== 'ok')
                    {

                    }

                    else
                        alert('مقادیر معتبر ارسال کنید')

                },
                error: function (msg) {
                    console.log(msg)
                }
            })
        })



        function Spinner(_this,status) {

            if(status== 'off')
            {
                _this.children('#buttonSpinner').fadeOut()
            }
            if(status== 'on')
            {
                _this.children('#buttonSpinner').fadeIn()
            }
        }

        function findId(_this) {
            var jum= _this.parents('.jumbotron')
            var id= jum.attr('data-id')

            return [id,jum]
        }

        $('input[type=file]').on('change',function(){
            //get the file name
            var fileName = $(this).val();
            //replace the "Choose a file" label
            $(this).next('.custom-file-label').html(fileName);
        })


        function cuteHide(el) {
            el.animate({opacity:'0'},500,function () {
                el.addClass('NoPaddingMargin')
            })
            el.animate({height:'0'},500,function () {
                el.remove()
            })
        }

    </script>
@endsection


