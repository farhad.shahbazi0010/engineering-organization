<?php

use Illuminate\Support\Facades\Route;

Route::get('/','IndexController@index');
Route::get('/advertisement','IndexController@advertisement');
Route::post('/saveAds','IndexController@saveAds');


Route::get('/export/{section}','IndexController@export')->middleware('admin');



Route::get('/loginRegister','IndexController@loginRegister')->middleware('guest');
Route::get('/refreshcaptcha','IndexController@refreshcaptcha');
Route::post('/adspagination/{pagenumber}/{section}','IndexController@adspagination');



Route::get('/news','IndexController@blog');
Route::get('/news/{id}','IndexController@singleblog');

Route::get('/anjoman','IndexController@association');


Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');




Route::get('/admin/ads', 'AdvertisementController@show_ads')->middleware('admin');
Route::post('/admin/ads_approval/{id}', 'AdvertisementController@ads_approval')->middleware('admin');
Route::post('/admin/ads_disapproval/{id}', 'AdvertisementController@ads_disapproval')->middleware('admin');
Route::get('/admin/done_ads', 'AdvertisementController@show_done_ads')->middleware('admin');

Route::get('/admin/user', 'AdminController@show_user')->middleware('admin');
Route::get('/admin/manager', 'AdminController@show_manager')->middleware('admin');

Route::post('/admin/ads/delete/{id}', 'AdvertisementController@deleteAds')->middleware('admin');
Route::post('/admin/changeAds/{id}', 'AdvertisementController@changeAds')->middleware('admin');
Route::post('/admin/change_done_ads/{id}', 'AdvertisementController@change_done_ads')->middleware('admin');
Route::post('/admin/user/delete', 'AdminController@delete')->middleware('admin');
Route::post('/admin/changeToAdmin', 'AdminController@changeToAdmin')->middleware('admin');


Route::get('/admin/blog', 'BlogController@blog')->middleware('admin');
Route::post('/admin/blog/save', 'BlogController@save')->middleware('admin');
Route::post('/admin/blog/update/{id}', 'BlogController@update')->middleware('admin');
Route::post('/admin/blog/delete/{id}', 'BlogController@delete')->middleware('admin');
Route::post('/blogPagination/{pagenumber}', 'BlogController@blogPagination')->middleware('admin');




Route::get('/admin/message', 'MessageController@index')->middleware('admin');
Route::post('/message/save', 'MessageController@save');


Route::post('/admin/message/delete/{id}', 'MessageController@delete')->middleware('admin');
Route::get('/admin/association', 'AssociationController@association')->middleware('admin');
Route::post('/admin/association/update', 'AssociationController@update')->middleware('admin');



