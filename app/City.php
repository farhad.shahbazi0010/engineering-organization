<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function regions()
    {
        return $this->hasMany('App\City','city_id');
    }

    public function cityAdvertisements()
    {
        return $this->hasMany('App\Advertisement');
    }
    public function regionAdvertisements()
    {
        return $this->hasMany('App\Advertisement','region_id');
    }
}
