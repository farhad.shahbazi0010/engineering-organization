<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{

    protected $guarded=['captcha','is_done','admin_approval'];
    public function city()
    {
        return $this->belongsTo('App\City');
    }
    public function region()
    {
        return $this->belongsTo('App\City','region_id');
    }
}
