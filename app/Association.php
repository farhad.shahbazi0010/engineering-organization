<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Association extends Model
{
    public $timestamps= false;
}
