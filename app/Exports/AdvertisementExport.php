<?php


namespace App\Exports;

use App\Advertisement;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Morilog\Jalali\Jalalian;

class AdvertisementExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    public function collection()
    {
        $ads= Advertisement::orderBy('id','DESC')->get();
        foreach ($ads as $index => $ad) {
            $name= $ad->name;
            $lastname= $ad->lastname;
            $area= $ad->area;
            $cstage= $ad->cstage;
            $ceil= $ad->ceil;
            $negative= $ad->negative;
            $city= $ad->city->title;
            @$region= $ad->region->title;
            $type= $ad->type;
            $phone= $ad->phone;
            $created_at= Jalalian::forge($ad->created_at)->format('Y/m/d');
            if($ad->admin_approval == 1 && $ad->is_done == 1)
                $status= 'انجام شده';
            elseif($ad->admin_approval == 1 && $ad->is_done == 0)
                $status= 'تایید شده';
            else
                $status= 'تایید نشده';

            $data[$index]= [$name, $lastname, $area, $cstage, $ceil, $negative, $city, $region, $type, $phone, $created_at, $status];
        }

        $order= ['تایید نشده', 'تایید شده', 'انجام شده'];
        usort($data, function ($x, $y) use ($order){
            $xOrder = array_search(end($x),$order);
            $yOrder = array_search(end($y),$order);

            if ($xOrder > $yOrder)
                return 1;

            return 0;
        });

        return new Collection($data);
    }

    public function headings(): array
    {
        return ['نام','نام خانوادگی','متراژ','مرحله ساختمانی','تعداد سقف','تعداد منفی','شهر','منطقه','نوع','شماره تلفن','تاریخ ثبت','وضعیت'];
    }
}
