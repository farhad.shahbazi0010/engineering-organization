<?php


namespace App\Exports;


use App\User;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Morilog\Jalali\Jalalian;

class UserExport implements FromCollection, WithHeadings, ShouldAutoSize
{

    public function collection()
    {
        $users= User::where('role','engineer')->orderBy('id','DESC')->get();
        foreach ($users as $index => $user) {
            $fullname= $user->fullname;
            $phone= $user->phone;
            $created_at= Jalalian::forge($user->created_at)->format('Y/m/d');

            $data[$index]= [$fullname, $phone, $created_at];
        }

        return new Collection($data);
    }

    public function headings(): array
    {
        return ['نام و نام خانوادگی', 'شماره تلفن', 'تاریخ عضویت'];
    }
}
