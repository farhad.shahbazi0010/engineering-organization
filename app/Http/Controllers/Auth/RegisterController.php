<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fullname' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'numeric', 'digits:11', 'unique:users'],
            'password' => ['required', 'string', 'min:5', 'confirmed'],
            'captcha' => ['required', 'captcha'],
        ],
        [
            'phone.digits'=> ' شماره تلفن معتبر وارد کنید',
            'require'=> 'تمامی فیلدها را پر کنید' ,
            'phone.numeric' => 'مقدار عددی وارد کنید' ,
            'password.confirmed' => 'رمزعبور ها مطابقت ندارند' ,
            'password.min' => 'حداقل 5 کاراکتر برای رمزعبور وارد کنید',
            'phone.unique'=> 'این شماره تلفن از قبل استفاده شده است',
            'captcha'=> 'مقدار صحیح وارد کنید'

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        $user= new User(
            [
                'fullname' => $data['fullname'],
                'phone' => $data['phone'],
                'password' => Hash::make($data['password'])
            ]
        );
        $user->role = 'engineer';
        $user->save();
        return $user;
//        return User::create([
//            'fullname' => $data['fullname'],
//            'phone' => $data['phone'],
//            'role' => 'engineer',
//            'password' => Hash::make($data['password']),
//        ]);
    }

    public function showRegistrationForm()
    {
        abort(404);
    }


}
