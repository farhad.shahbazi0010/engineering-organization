<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticated(Request $request , $user)
    {
        if ($user->role=='engineer')
            return redirect('/');
        if ($user->role== 'admin')
            return redirect('/admin/ads');
    }
    public function username()
    {
        $phone= request()->input('phone2');
        request()->merge(['phone'=> $phone]);
        return 'phone';
    }

    public function sendFailedLoginResponse()
    {
        return redirect()->back()->withErrors(['credential'=> 'شماره تلفن یا رمزعبور اشتباه است'])->withInput(request()->only('phone2'));
    }

    public function showLoginForm()
    {
        abort(404);
    }


}
