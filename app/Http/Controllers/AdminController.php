<?php

namespace App\Http\Controllers;

use App\Advertisement;
use App\User;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function show_user()
    {
        $users= User::where('id','!=',Auth::id())->where('role','engineer')->orderBy('id','DESC')->get();
        return view('admin/user/index',compact('users'));
    }
    public function show_manager()
    {
        $managers= User::where('id','!=',Auth::id())->where('role','admin')->orderBy('id','DESC')->get();
        return view('admin/manager/index',compact('managers'));
    }

    public function delete(Request $request)
    {
        if (empty($request->ids))
            return response()->json(['status'=>'not ok']);
        User::whereIn('id',$request->ids)->delete();
        return response()->json(['status'=>'ok']);
    }

    public function changeToAdmin(Request $request)
    {
       if (empty($request->ids))
           return response()->json(['status'=>'not ok']);

        if ($request->change == 1)
        {
            User::whereIn('id',$request->ids)->update(['role'=>'admin']);
        }
        else
        {
            User::whereIn('id',$request->ids)->update(['role'=>'engineer']);
        }
        return response()->json(['status'=>'ok']);
    }
}
