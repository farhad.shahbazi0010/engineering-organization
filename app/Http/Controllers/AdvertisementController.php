<?php

namespace App\Http\Controllers;

use App\Advertisement;
use Illuminate\Http\Request;

class AdvertisementController extends Controller
{
    public function show_ads()
    {
        $showItem=1;
        $allads= Advertisement::where('is_done',0)->orderBy('id','DESC');
        $ads= $allads->skip(0)->take(6)->get();
        $adsCount= $allads->count();
        return view('admin/ads/index',compact('ads','adsCount','showItem'));
    }

    public function ads_approval($id)
    {
        $ads= Advertisement::find($id);
        $ads->admin_approval= 1;
        $ads->save();
    }
    public function ads_disapproval($id)
    {
        $ads= Advertisement::find($id);
        $ads->admin_approval= 0;
        $ads->save();
    }
    public function show_done_ads()
    {
        $showItem=1;
        $alldone_ads= Advertisement::where('is_done',1)->orderBy('id','DESC');
        $done_ads= $alldone_ads->skip(0)->take(6)->get();
        $done_adsCount= $alldone_ads->count();
        return view('admin/done_ads/index',compact('done_ads','done_adsCount','showItem'));
    }

    public function deleteAds($id)
    {
        $ad= Advertisement::find($id)->delete();
        return response()->json(['status'=>'ok']);
    }

    public function changeAds($id)
    {
        Advertisement::where('id',$id)->update(['is_done'=>1]);
        return response()->json(['status'=>'ok']);
    }
    public function change_done_ads($id)
    {
        Advertisement::where('id',$id)->update(['is_done'=>0]);
        return response()->json(['status'=>'ok']);
    }
}
