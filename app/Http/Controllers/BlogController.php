<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class BlogController extends Controller
{
    public function blog()
    {
        $allblogs= Blog::orderBy('id','DESC');

        $blogs= $allblogs->skip(0)->take(6)->get();
        $count= $allblogs->count();

        return view('admin/blog/index',compact('blogs','count'));
    }

    public function save(Request $request){

        $validator= Validator::make($request->all(),[
            'text'=> 'required',
            'title'=> 'required',
        ]);

        if ($validator->fails())
            return response()->json(['status'=>'not ok']);



        $blog= new Blog($request->all());
        $blog->save();

        if(!empty($request->file))
            $request->file('file')->move('blog/'.$blog->id, 'blog_'.$blog->id.'.jpg');


        return response()->json(['status'=>'ok','id'=>$blog->id]);

    }

    public function update($id, Request $request)
    {
        $validator= Validator::make($request->all(),[
            'title'=>'required',
            'text'=> 'required'
        ]);

        if ($validator->fails())
            return response()->json(['status'=>'not ok']);


        $blog= Blog::where('id',$id)->update(['title'=>$request->title, 'text'=>$request->text]);

        if(!empty($request->file))
            $request->file('file')->move('blog/'.$id, 'blog_'.$id.'.jpg');

        return ['status'=>'ok'];

    }

    public function delete($id)
    {

        if (file_exists(public_path('blog/'.$id.'/blog_'.$id.'.jpg'))){
            unlink(public_path('blog/'.$id.'/blog_'.$id.'.jpg'));
            rmdir(public_path('blog/'.$id));

        }


        Blog::where('id',$id)->delete();
        return response()->json(['status'=>'ok']);
    }

    public function blogPagination($pagenumber)
    {

        $showItem= request()->showItem;
        $skip= ($pagenumber - 1) * 6;
        $take=  6;
        $allblogs= Blog::orderBy('id','DESC');

        $blogs= $allblogs->skip($skip)->take($take)->get();

//        foreach ($blogs as $key=>$blog) {
//            $blogs[$key]->text= mb_substr($blog->text,0,430,'UTF-8');
//        }

        return view('index/blogLayout/index',compact('blogs','showItem'));

    }
}
