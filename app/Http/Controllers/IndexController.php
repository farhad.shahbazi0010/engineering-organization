<?php

namespace App\Http\Controllers;

use App\Advertisement;
use App\Association;
use App\Blog;
use App\City;
use App\Exports\AdvertisementExport;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use Morilog\Jalali\Jalalian;
use App\Exports\UserExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Collection;

class IndexController extends Controller
{
    public function index()
    {
        $showItem=0;
        $all_ads= Advertisement::where('is_done',0)->where('admin_approval',1)->orderBy('id','DESC');
        $ads= $all_ads->skip(0)->take(6)->get();
        $adsCount= $all_ads->count();

        $all_done_ads= Advertisement::where('is_done',1)->where('admin_approval',1)->orderBy('id','DESC');
        $done_ads= $all_done_ads->skip(0)->take(6)->get();
        $done_adsCount= $all_done_ads->count();

        return view('index/index',compact('ads','done_ads','adsCount','done_adsCount','showItem'));
    }

    public function advertisement()
    {

        $cities= City::where('city_id',null)->orderBy('ordering', 'ASC')->get();

        return view('index/advertisement',compact('cities'));
    }

    public function saveAds(Request $request)
    {

        $validator= $request->validate([
            'name'=>'required',
            'lastname'=>'required',
            'ceil'=>'required|numeric',
            'negative'=>'required|numeric',
            'area'=>'required|numeric',
            'cstage'=>'required|in:شروع به کار,فوندانسیون,در حال ساخت,سفت کاری,نازک کاری,پایان کار',
//            'price'=>'required|numeric',
            'phone'=>'required|digits:11|numeric',
            'type'=>'required|in:حقیقی,حقوقی,حقیقی و حقوقی',
            'city_id'=>[Rule::exists('cities','id')->whereNull('city_id')],
            'region_id'=>[Rule::exists('cities','id')->whereNotNull('city_id')],
            'captcha'=> 'required|captcha'

        ],
        [
            'require'=> 'فیلد بالا را پر کنبد',
            'numeric'=> 'مقدار عددی وارد کنید',
            'phone.digits'=> 'شماره تلفن باید 11 رقم باشد',
            'in'=> 'گزینه معتبر انتخاب کنید',
            'captcha'=> 'مقدار صحیح وارد کنید',
            'exists'=> 'گزینه معتبر انتخاب کنید',
            'required'=> 'این فیلد باید پر شود'
        ]);

        $city= City::find($request->city_id);
        $advertisement= new Advertisement($request->all());
        $advertisement->city()->associate($city);

        if ($city->title== 'تهران')
        {
            $region= City::find($request->region_id);
            $advertisement->region()->associate($region);

        }
        else
        {
            $advertisement->region_id= null;
        }
        $advertisement->is_done=0;
        $advertisement->save();
        return redirect('/advertisement')->with('status', 'ok');


    }

    public function loginRegister()
    {
        return view('index/loginRegister');
    }

    public function refreshcaptcha()
    {
        return response()->json(['captcha'=>captcha_img('math')]);
    }


    public function adspagination($pagenumber,$section)
    {
        $showItem= request()->showItem;

        if (Auth::check() && Auth::user()->role=='admin')
            $is_admin= 1;

        $skip= ($pagenumber-1)*6;
        $take= $pagenumber*6;
        if ($section== 0)
        {
            if ($showItem== 1)
                $ads= Advertisement::where('is_done', 0)->orderBy('id','DESC')->skip($skip)->take($take)->get();
            else
                $ads= Advertisement::where('is_done', 0)->where('admin_approval',1)->orderBy('id','DESC')->skip($skip)->take($take)->get();

            return view('index/ads_done_ads/ads',compact('ads','showItem'));
        }
        else
        {
            if ($showItem== 1)
                $done_ads= Advertisement::where('is_done', 1)->orderBy('id','DESC')->skip($skip)->take($take)->get();
            else
                $done_ads= Advertisement::where('is_done', 1)->where('admin_approval',1)->orderBy('id','DESC')->skip($skip)->take($take)->get();

            return view('index/ads_done_ads/done_ads',compact('done_ads','showItem'));
        }
    }


    public function blog()
    {
        $showItem=0;
        $allblogs= Blog::orderBy('id','DESC');
        $blogs= $allblogs->skip(0)->take(6)->get();
//        foreach ($blogs as $key=>$blog) {
//            $blogs[$key]->text= mb_substr($blog->text,0,830,'UTF-8');
//        }
        $count= $allblogs->count();
        return view('index/blog',compact('blogs','count','showItem'));
    }

    public function singleblog($id)
    {
        $blog= Blog::where('id',$id)->first();
        return view('index/singleBlog',compact('blog'));
    }

    public function association()
    {
        $asso= Association::first();
        return view('index/association',compact('asso'));
    }

    public function export($section)
    {
        if ($section== 1)
            return Excel::download(new AdvertisementExport, 'advertisements.xlsx');
        else
            return Excel::download(new UserExport, 'Users.xlsx');
    }

}
