<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    public function index()
    {
        Message::where('is_read',0)->update(['is_read'=>1]);
        $messages= Message::orderBy('id','DESC')->get();
        return view('admin/message/index',compact('messages'));
    }

    public function delete($id)
    {
        Message::where('id',$id)->delete();
        return response()->json(['status'=>'ok']);

    }

    public function save(Request $request)
    {
        $message= new Message();
        $message->text= $request->text;
        if (Auth::check())
        {
            $user= Auth::user();
            $message->user()->associate($user);
        }

        $message->save();
    }
}
