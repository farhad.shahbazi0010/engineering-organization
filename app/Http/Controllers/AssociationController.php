<?php

namespace App\Http\Controllers;

use App\Association;
use Illuminate\Http\Request;

class AssociationController extends Controller
{
    public function association()
    {
        $asso=Association::first();
        return view('admin/association/index', compact('asso'));
    }

    public function update(Request $request)
    {
        $asso= Association::first();
        $asso->title= $request->title;
        $asso->text= $request->text;
        $asso->save();

        return response()->json(['status'=>'ok']);
    }
}
